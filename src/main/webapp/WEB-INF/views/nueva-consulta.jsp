<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Nueva Consulta</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>
<body>
	<div class="container mt-2">
		<jsp:include page="Header.jsp" />
		<div class="d-flex bg-highlight mb-3">
			<div class="p-2">
				Estamos listos para proporcionarte las mejores recomendaciones para
				tu siguiente viaje. <br> A continuación te presentamos una
				lista tipos de atracciones. Selecciona los que quieres incluir en tu
				itinerario.
			</div>
		</div>
		<div class="d-flex flex-column bg-highlight mb-3">
			<form:form method="POST" action="consulta"
				modelAttribute="formSeleccion">
				<div class="form-group">
					<c:set var="no_opcion" value="1" />
					<c:forEach var="emp" items="${formSeleccion.catalogo}">
						<div class="p-2">
							<div class="custom-control custom-checkbox">
								<form:checkbox path="seleccionTipos" value="${emp.tipoAtraccionId}" cssClass="custom-control-input"/>
								<label class="custom-control-label" for="seleccionTipos${no_opcion}">${emp.tipoAtraccion}</label>						
							</div>
						</div>
						<c:set var="no_opcion" value="${no_opcion + 1}" />
					</c:forEach>
				</div>
				<button type="submit" class="btn btn-primary">Siguiente</button>
			</form:form>
		</div>
	</div>
</body>
</html>