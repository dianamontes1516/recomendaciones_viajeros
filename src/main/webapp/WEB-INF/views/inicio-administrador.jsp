<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Inicia Sesi�n</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />">

</script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>

<body>
	<div class="card bg-dark text-black">
		<img class="card-img" src="<c:url value="/resources/bacalar.jpg"/>"
			alt="Card image">
		<div class="card-img-overlay">
			<div class="container mt-2">
				<jsp:include page="Header.jsp" />
			</div>

			<br> <br> <br>

			<div class="d-flex justify-content-center">
				<div class="p-3 mb-2 bg-dark text-white">
					<form:form method="POST" action="inicioSesion"
						modelAttribute="frmUsuarioAdmin">
						<form:errors path="*" class="alert alert-danger" element="div" />
						<table>
							<tr>
								<td><form:label path="usuario">Usuario</form:label></td>
								<td><form:input path="usuario" /></td>
								<td><form:errors path="usuario" cssClass="error" /></td>
							</tr>
							<tr>
								<td><form:label path="contrase�a">Contrase�a </form:label></td>
								<td><form:password path="contrase�a" /></td>
								<td><form:errors path="contrase�a" cssClass="error" /></td>
							</tr>
							<tr>
								<td colspan="2"><input type="submit" value="Ingresar" /></td>
							</tr>
						</table>
					</form:form>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
