<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Resultados Consulta</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">
<link href="<c:url value="/webjars/font-awesome/css/fontawesome.css"/>"
	rel="stylesheet">
<link href=""
	<c:url value="/webjars/font-awesome/css/brands.css"/>" rel="stylesheet">
<link href="<c:url value="/webjars/font-awesome/css/solid.css"/>"
	rel="stylesheet">
</head>
<body>
	<div class="container mt-2">
		<jsp:include page="Header.jsp" />
		<div class="d-flex">
			<div class="p-1 m-3">
				<h2>Escoge la propuesta que más te agrade!</h2>
			</div>
		</div>

		<div class="accordion" id="accordionExample">
			<div class="card">
				<c:set var="no_opcion" value="1" />
				<c:forEach var="individuo" items="${formSeleccionIndividuo.individuos}">
					<div class="card-header bg-info text-white"
						id="heading_${no_opcion }">
						<div class="d-flex">
							<div class="m-1">Opción ${no_opcion }</div>
							<div class="ml-auto">
								<button class="btn btn-link" type="button"
									data-toggle="collapse" data-target="#collapse_${no_opcion }"
									aria-expanded="false" aria-controls="collapse_${no_opcion }">
									<span style="font-size: 1em; color: white;"> <i
										class="fas fa-chevron-circle-down"></i>
									</span>
								</button>
							</div>
						</div>
					</div>

					<div id="collapse_${no_opcion }" class="collapse"
						aria-labelledby="heading_${no_opcion }"
						data-parent="#accordionExample">
						<div class="card-body">
							<div class="container">
								<div class="row justify-content-center">
									<div class="col-md-3">

										<div id="carousel_${no_opcion}" class="carousel slide"
											data-ride="carousel">

											<div class="carousel-inner">
												<c:set var="primero" value="true" />
												<c:forEach var="atraccion" items="${individuo.getAtracciones()}">
													<c:choose>
														<c:when test="${primero}">
															<c:set var="primero" value="false" />
															<div class="carousel-item active">
														</c:when>
														<c:otherwise>
															<div class="carousel-item">
														</c:otherwise>
													</c:choose>
														<img src="<c:url value="/resources/gris.jpg"/>"
															class="d-block w-100" alt="...">
														<div class="carousel-caption d-none d-md-block">
															<h5>${atraccion.nombre}</h5>
<%-- 														<p>${atraccion.getNombreTipoAtraccion()}</p> --%>
														</div>
											    	</div>
				                                </c:forEach>
											</div>

										<a class="carousel-control-prev" href="#carousel_${no_opcion }"
				role="button" data-slide="prev"> <span
				class="carousel-control-prev-icon" aria-hidden="true"></span> <span
				class="sr-only">Previous</span>
			</a> 
										<a class="carousel-control-next" href="#carousel_${no_opcion }"
				role="button" data-slide="next"> <span
				class="carousel-control-next-icon" aria-hidden="true"></span> <span
				class="sr-only">Next</span>
			</a>
									</div>
								</div>
							<div class="col-md-1">
							    <form:form method="POST" action="hospedaje" modelAttribute="formSeleccionIndividuo">
							        
							        <form:input path="seleccion" value="${individuo.idsAtrString()}" type="hidden" />
							        <button type="sumit" class="btn btn-info"> Seleccionar </button>
							    </form:form>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<c:set var="no_opcion" value="${no_opcion + 1}" />
			</c:forEach>
			
		</div>
 	</div>

	</div>
</body>
</html>