<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Viajes</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"> </script>
<link rel="stylesheet" href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>
<body>
	<div class="container mt-2">
		<jsp:include page="Header.jsp" />
		<p>Mapeo correcto</p>
	</div>
</body>
</html>