<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Atracciones</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">
<link href="<c:url value="/webjars/font-awesome/css/fontawesome.css"/>"
	rel="stylesheet">
<link href=""
	<c:url value="/webjars/font-awesome/css/brands.css"/>" rel="stylesheet">
<link href="<c:url value="/webjars/font-awesome/css/solid.css"/>"
	rel="stylesheet">

</head>
<body>

	<div class="container">
		<div class="row">
			<div class="col">
				<jsp:include page="HeaderAdministrador.jsp" />
			</div>
		</div>

		<div class="row mt-2 justify-content-end">
			<div class="col-md-2">
				<a
					href="/recomendaciones-viajeros/atraccion/nueva"
					class="btn btn-primary"> <i class="fas fa-plus-square"></i>
				</a>
			</div>
		</div>
<form:errors path="*" class="alert alert-danger" element="div" />
		<div class="row mt-2">
			<div class="col">
				<table class="table table-hover">
					<thead class="thead-dark">
						<tr>
							<th scope="col">#</th>
							<th scope="col">Nombre</th>
							<th scope="col">Tipo</th>
							<th scope="col">Ubicación</th>
							<th scope="col">Costo</th>
							<th scope="col">Acciones</th>
						</tr>
					</thead>
					<tbody>
						<c:set var="no_opcion" value="1" />
						<c:forEach var="atraccion" items="${atracciones}">
							<tr>
								<th scope="row">${no_opcion}</th>
								<td>${atraccion.nombre }</td>
								<td>${atraccion.getNombreTipoAtraccion() }</td>
								<td>${atraccion.longitud},${atraccion.latitud}</td>
								<td>${atraccion.costo}</td>
								<td>
									<button type="button" class="btn btn-success"
										data-toggle="modal" data-target="#modal${no_opcion }">
										<i class="fas fa-edit"></i>
									</button> <!-- Modal -->
									<div class="modal fade" id="modal${no_opcion }" tabindex="-1"
										role="dialog" aria-labelledby="exampleModalCenterTitle"
										aria-hidden="true">
										<div class="modal-dialog modal-dialog-centered"
											role="document">
											<div class="modal-content">
												<div class="modal-header">
													<h5 class="modal-title" id="exampleModalLongTitle">Edición</h5>
													<button type="button" class="close" data-dismiss="modal"
														aria-label="Close">
														<span aria-hidden="true">&times;</span>
													</button>
												</div>
												<div class="modal-body">

													<form:form method="POST"
														action="/recomendaciones-viajeros/atraccion/edita"
														modelAttribute="edicion">
														<form:input type="hidden" path="atraccionId"
															value="${atraccion.atraccionId}" />
														<div class="form-group row justify-content-center">
															<label for="formNombre" class="col-sm-4 col-form-label">Nombre*</label>
															<div class="col-sm-8">
																<form:input class="form-control" id="formNombre"
																	path="nombre" value="${atraccion.nombre }" />
															</div>
														</div>
														<div class="form-group row justify-content-center">
															<label for="formLatitud" class="col-sm-4 col-form-label">Latitud*</label>
															<div class="col-sm-8">
																<form:input class="form-control" id="formLatitud"
																	path="latitud" value="${atraccion.latitud}" />
															</div>
														</div>
														<div class="form-group row justify-content-center">
															<label for="formLongitud" class="col-sm-4 col-form-label">Longitud*</label>
															<div class="col-sm-8">
																<form:input class="form-control" id="formLongitud"
																	path="longitud" value="${atraccion.longitud}" />
															</div>
														</div>
														<div class="form-group row justify-content-center">
															<label for="formCosto" class="col-sm-4 col-form-label">Costo</label>
															<div class="col-sm-8">
																<form:input class="form-control" id="formCosto"
																	path="costo" value="${atraccion.costo}" />
															</div>
														</div>
														<div class="form-group row justify-content-center">
															<label for="formTipoA" class="col-sm-4 col-form-label">Tipo
																Atracción*</label>
															<div class="col-sm-8">
																<form:select path="TipoAtraccionId" class="form-control">
																	<c:forEach var="item" items="${catalogoTipos}">
																		<c:choose>

																			<c:when
																				test="${item.tipoAtraccionId == atraccion.tipoAtraccionId}">
																				<form:option value="${item.tipoAtraccionId}"
																					selected="selected"> 
  																	    ${item.tipoAtraccion }
  																    </form:option>

																			</c:when>

																			<c:otherwise>
																				<form:option value="${item.tipoAtraccionId}"> 
  																	    ${item.tipoAtraccion }
  																    </form:option>

																			</c:otherwise>
																		</c:choose>
																	</c:forEach>
																</form:select>
															</div>
														</div>
														<div class="row justify-content-center">
															<div clas="col-sm-1">
																<button type="button" class="btn btn-secondary"
																	data-dismiss="modal">Cerrar</button>
																<button type="Submit" class="btn btn-primary">Guardar</button>

															</div>
														</div>

													</form:form>

												</div>

											</div>
										</div>
									</div> <a
									href="/recomendaciones-viajeros/atraccion/elimina/${atraccion.atraccionId}"
									class="btn btn-danger"> <i class="fas fa-trash-alt"></i>
								</a>
								</td>
							</tr>
							<c:set var="no_opcion" value="${no_opcion + 1}" />
						</c:forEach>
					</tbody>
				</table>
			</div>
		</div>
	</div>




</body>

</html>