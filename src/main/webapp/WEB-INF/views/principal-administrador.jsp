<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1" isELIgnored="false"%>

<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Principal Administrador</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />">

</script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>

<body>

	<div class="card bg-dark text-black">
		<img class="card-img" src="<c:url value="/resources/bacalar.jpg"/>"
			alt="Card image">
		<div class="card-img-overlay">
			<div class="container mt-2">
				<jsp:include page="HeaderAdministrador.jsp" />
			</div>

			<br> <br> <br>

			<div class="d-flex justify-content-center">
				<h1>Bienvenid@ ${usuarioFirmado.usuario}</h1>
			</div>

			<br> <br> <br>

			<div class="row card-columns justify-content-center">
				<div class="col-md-3">
					<div class="card text-white bg-dark" >
						<div class="card-header">
							<a class="nav-link"
								href="/recomendaciones-viajeros/atraccion/lista">Ve
								atracciones</a>
						</div>
						<div class="card-body">
							<h5 class="card-title">Atracciones</h5>
							<p class="card-text">Administra las atracciones.</p>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="card text-white bg-dark" >
						<div class="card-header">
							<a class="nav-link"
								href="/recomendaciones-viajeros/administrador-hospedaje/hospedajes">Ve hoteles</a>
						</div>
						<div class="card-body">
							<h5 class="card-title">Hoteles</h5>
							<p class="card-text">Administra los hoteles.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
</body>

</body>
</html>
