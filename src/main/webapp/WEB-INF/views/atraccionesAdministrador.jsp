<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Atracciones</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"> </script>
<link rel="stylesheet" href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>

<body>
<div class="card bg-dark text-black">
  <img class="card-img" src="<c:url value="/resources/bacalar.jpg"/>" alt="Card image">
  <div class="card-img-overlay">
	<div class="container mt-2">
		<jsp:include page="HeaderAdministrador.jsp" />
	</div>
	
   <br> 
   <br>
   <br>
	<div class="d-flex justify-content-center">
	<h1>Bienvenid@ ${usuarioFirmado.usuario}</h1>
	</div>
	
		<br>
	<br>
	<br>
<form:form method="POST" action="atracciones/listar">

	<p>Atracciones en la Base de datos: </p>
	<p>
		<table>
			<thead>
				<tr>
					<td>Clave Atracci�n</td>
					<td>Nombre Atracci�n</td>
					<td>Latitud</td>
					<td>Longitud</td>
					<td>Costo</td>
				</tr>
			</thead>
		<c:forEach var="atraccion_actual" items="${atracciones}">
			<tr>
				<td>${atraccion_actual.atraccionId}</td>	
				<td>${atraccion_actual.nombre}</td>
				<td>${atraccion_actual.latitud}</td>
				<td>${atraccion_actual.longitud}</td>
				<td>${atraccion_actual.costo}</td>
			</tr>
		</c:forEach>
		</table>
	</form:form>
	</div>
	</div>
</body>
</html>