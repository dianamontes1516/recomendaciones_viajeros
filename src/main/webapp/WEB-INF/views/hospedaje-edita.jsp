<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Editar Hospedaje</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"> </script>
<link rel="stylesheet" href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>
<body>

	<div class="container mt-2">
		<jsp:include page="HeaderAdministrador.jsp" />
	</div>
	
	<br>
	<br>
	<br>
	
	<p>Actualiza el hospedaje ${hospedajeSeleccion.nombre}</p>
	
<form:form method="POST" action="actualiza-hospedaje" modelAttribute="frmHospedaje">
<form:input path="seleccion" value="${hospedajeSeleccion.hospedajeId}" type="hidden"/>
<form>
  <div class="form-row">
    <div class="col-md-4 mb-3">
      <label for="nombre" id="nom">Nombre Hospedaje</label>
      <form:input path="nombre" type="text" class="form-control" id="nombre" placeholder="nombre" value="${hospedajeSeleccion.nombre}" required="true"/>
      <form:errors path="nombre" cssClass="error" />
    </div>
    <div class="col-md-4 mb-3" id="costo">
      <label for="costo">Costo</label>
      <form:input path="costo" type="number" class="form-control" id="costo" placeholder="costo" value="${hospedajeSeleccion.costo}" required="true"/>
      <form:errors path="costo" cssClass="error" />
    </div>
  </div>
  
  <div class="form-row">
    <div class="col-md-6 mb-3">
      <label for="latitud">Latitud</label>
      <form:input path="latitud" type="text" class="form-control" id="latitud" placeholder="latitud" value="${hospedajeSeleccion.latitud}" required="true" />
      <form:errors path="latitud" cssClass="error" />
      <div class="invalid-tooltip">
        N�meros �nicamente.
      </div>
    </div>
    <div class="col-md-3 mb-3">
      <label for="longitud">Longitud</label>
      <form:input path="longitud" type="text" class="form-control" id="longitud" placeholder="longitud" value="${hospedajeSeleccion.longitud}" required="true"/>
      <form:errors path="longitud" cssClass="error" />
      <div class="invalid-tooltip">
        N�meros �nicamente.
      </div>
    </div>
  </div>
  
  <button class="btn btn-primary" type="submit">Actualizar</button>
</form>
</form:form>

<form:form method="POST" action="elimina-hospedaje" modelAttribute="frmHospedaje">
	<form:input path="seleccion" value="${hospedajeSeleccion.hospedajeId}" type="hidden"/>
	<button type="submit" class="btn btn-danger"> Eliminar </button>
</form:form>	


</body>
</html>