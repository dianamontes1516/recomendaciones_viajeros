<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Hospedajes</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>
<body>
	<div class="container mt-2">
		<jsp:include page="Header.jsp" />
		<div class="d-flex bg-highlight mb-3">
			<div class="p-2">A continuación enlistamos los hospedajes que
				coinciden con la recomendación que elegiste:</div>
		</div>

		<div class="row">
			<div class="col-4">
				<div class="list-group list-group-flush" id="list-tab"
					role="tablist">
					<c:forEach var="hospedaje" items="${hospedajes}">
						<a class="list-group-item list-group-item-action"
							id="list-home-list" data-toggle="list"
							href="#list-${hospedaje.hospedajeId}" role="tab"
							aria-controls="home">${hospedaje.nombre}</a>
					</c:forEach>
				</div>
			</div>
			<div class="col-5">
				<div class="tab-content" id="nav-tabContent">
					<c:forEach var="hospedaje" items="${hospedajes}">
						<div class="tab-pane fade" id="list-${hospedaje.hospedajeId}"
							role="tabpanel" aria-labelledby="list-home-list">
							<ul>
								<li><b>Costo promedio por persona:</b> ${hospedaje.costo}</li>
								<li><b>Tipo de hospedaje:</b>
								 <c:forEach var="tipo" items="${hospedaje.relacionTipoHospedaje}">
									${tipo.tipoHospedaje}
								</c:forEach>
								</li>
							</ul>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
</body>
</html>