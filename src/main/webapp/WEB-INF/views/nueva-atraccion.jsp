<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<title>Nueva Atracción</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>
<body>
	<div class="container mt-2">
		<jsp:include page="HeaderAdministrador.jsp" />
		<div class="d-flex bg-highlight mb-3">
			<div class="p-2">Introduce los campos para dar de alta una
				atracción:</div>
		</div>
		<div class="">
			<form:form method="POST"
				action="/recomendaciones-viajeros/atraccion/agrega-atraccion"
				modelAttribute="atraccion">
				<form:errors path="*" class="alert alert-danger" element="div" />
				<div class="form-group row justify-content-center">
					<label for="formNombre" class="col-sm-2 col-form-label">Nombre*</label>
					<div class="col-sm-5">
						<form:input class="form-control" id="formNombre" path="nombre" />
					</div>
				</div>
				<div class="form-group row justify-content-center">
					<label for="formLatitud" class="col-sm-2 col-form-label">Latitud*</label>
					<div class="col-sm-5">
						<form:input class="form-control" id="formLatitud" path="latitud" />
					</div>
				</div>
				<div class="form-group row justify-content-center">
					<label for="formLongitud" class="col-sm-2 col-form-label">Longitud*</label>
					<div class="col-sm-5">
						<form:input class="form-control" id="formLongitud" path="longitud" />
					</div>
				</div>
				<div class="form-group row justify-content-center">
					<label for="formCosto" class="col-sm-2 col-form-label">Costo</label>
					<div class="col-sm-5">
						<form:input class="form-control" id="formCosto" path="costo" />
					</div>
				</div>
				<div class="form-group row justify-content-center">
					<label for="formTipoA" class="col-sm-2 col-form-label">Tipo
						Atracción*</label>
					<div class="col-sm-5">
						<form:select path="TipoAtraccionId" class="form-control">
							<c:forEach var="item" items="${catalogoTipos}">
								<form:option value="${item.tipoAtraccionId}"> ${item.tipoAtraccion } </form:option>
							</c:forEach>
						</form:select>
					</div>
				</div>
				<div class="row justify-content-center">
					<div>
						<a href="/recomendaciones-viajeros/atraccion/lista"
							class="btn btn-secondary"> Regresar
						</a>
					</div>

					<div class="col-sm-1">
						<button type="Submit" class="btn btn-primary">Crear</button>
					</div>

				</div>
			</form:form>
		</div>
	</div>
</body>
</html>