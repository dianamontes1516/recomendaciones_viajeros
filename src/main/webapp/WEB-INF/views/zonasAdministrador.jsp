<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Zonas</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"> </script>
<link rel="stylesheet" href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>

<body>
<div class="container mt-2">
<div class="card bg-dark text-black">
  <img class="card-img" src="<c:url value="/resources/bacalar.jpg"/>" alt="Card image">
  <div class="card-img-overlay">
	<div class="container mt-2">
		<jsp:include page="HeaderAdministrador.jsp" />
	</div>


<%-- <form:form method="POST" action="atracciones/listar"> --%>

	<p>Zonas en la Base de datos: </p>
	<p>
	
<table class="table table-hover table-dark">
  <thead>
    <tr>
		<th scope="col">Clave Tipo Zona</th>
		<th scope="col">Nombre Zona</th>
	</tr>
		</thead>
		<tbody>
		<c:forEach var="tipoZona_actual" items="${tipoZonas}">
			<tr>
				<th scope="row">${tipoZona_actual.tipoZonaId}</th>	
				<td>${tipoZona_actual.tipoZona}</td>
			</tr>
		</c:forEach>
		</tbody>
		</table>
<%-- 	</form:form> --%>
<tr>
	<a class="btn btn-warning" href="/recomendaciones-viajeros/zonas/modificar">Agrega Zona</a>
</tr>


<!--   <button type="submit" class="btn btn-primary">Ingresar</button> -->
<%--   </form:form> --%>


</div>
</div>
</body>
</html>