<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8" isELIgnored="false"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Resultados Consulta</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>
<body>
	<div class="container mt-2">
		<jsp:include page="Header.jsp" />
		<div class="d-flex mb-3">
			<div class="p-1">
				<h2>Escoge la propuesta que más te agrade!</h2>
			</div>
		</div>
		<div class="accordion" id="accordionExample">
			<div class="card">
				<div class="card-header bg-info text-white" id="headingOne">
					<div class="d-flex">
						Opción #1
						<div class="ml-auto">
							<button class="btn btn-link" type="button" data-toggle="collapse"
								data-target="#collapseOne" aria-expanded="false" 
								aria-controls="collapseOne">Y</button>
						</div>
					</div>
				</div>
				<div id="collapseOne" class="collapse"
					aria-labelledby="headingOne" data-parent="#accordionExample">

					<div class="card-body">
						<div class="d-flex flex-column">
							<h4>Reservas Ecológicas</h4>
							<div class="p-1 justify-content-center">
								<div id="carouselExampleIndicators" class="carousel slide">
									<ol class="carousel-indicators">
										<li data-target="#carouselExampleIndicators" data-slide-to="0"
											class="active"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
										<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
									</ol>
									<div class="carousel-inner">
										<div class="carousel-item active">
											<img class="d-block img-thumbnail"
												src="<c:url value="/resources/bacalar.jpg"/>"
												alt="First slide">
											<div class="carousel-caption d-none d-md-block">
												<h5>Relájate</h5>
												<p>Nosotros encontramos un rinconcito de México para tu
													descanso.</p>
											</div>
										</div>
										<div class="carousel-item">
											<img class="d-block img-thumbnail"
												src="<c:url value="/resources/gastronomia.jpg"/>"
												alt="Second slide">
											<div class="carousel-caption d-none d-md-block">
												<h5>Relájate</h5>
												<p>Nosotros encontramos un rinconcito de México para tu
													descanso.</p>
											</div>
										</div>
										<div class="carousel-item">
											<img class="d-block img-thumbnail"
												src="<c:url value="/resources/sierragorda.jpg"/>"
												alt="Third slide">
											<div class="carousel-caption d-none d-md-block">
												<h5>Relájate</h5>
												<p>Nosotros encontramos un rinconcito de México para tu
													descanso.</p>
											</div>
										</div>
									</div>
									<a class="carousel-control-prev"
										href="#carouselExampleIndicators" role="button"
										data-slide="prev"> <span
										class="carousel-control-prev-icon" aria-hidden="true"></span>
										<span class="sr-only">Previous</span>
									</a> <a class="carousel-control-next"
										href="#carouselExampleIndicators" role="button"
										data-slide="next"> <span
										class="carousel-control-next-icon" aria-hidden="true"></span>
										<span class="sr-only">Next</span>
									</a>
								</div>

								<div class="p-1">
									<h4>Grutas</h4>
									<div class="d-flex justify-content-center">
										<div id="carouselExampleIndicators" class="carousel slide">
											<ol class="carousel-indicators">
												<li data-target="#carouselExampleIndicators"
													data-slide-to="0" class="active"></li>
												<li data-target="#carouselExampleIndicators"
													data-slide-to="1"></li>
												<li data-target="#carouselExampleIndicators"
													data-slide-to="2"></li>
											</ol>
											<div class="carousel-inner">
												<div class="carousel-item active">
													<img class="d-block img-thumbnail"
														src="<c:url value="/resources/bacalar.jpg"/>"
														alt="First slide">
													<div class="carousel-caption d-none d-md-block">
														<h5>Relájate</h5>
														<p>Nosotros encontramos un rinconcito de México para
															tu descanso.</p>
													</div>
												</div>
												<div class="carousel-item">
													<img class="d-block img-thumbnail"
														src="<c:url value="/resources/gastronomia.jpg"/>"
														alt="Second slide">
													<div class="carousel-caption d-none d-md-block">
														<h5>Relájate</h5>
														<p>Nosotros encontramos un rinconcito de México para
															tu descanso.</p>
													</div>
												</div>
												<div class="carousel-item">
													<img class="d-block img-thumbnail"
														src="<c:url value="/resources/sierragorda.jpg"/>"
														alt="Third slide">
													<div class="carousel-caption d-none d-md-block">
														<h5>Relájate</h5>
														<p>Nosotros encontramos un rinconcito de México para
															tu descanso.</p>
													</div>
												</div>
											</div>
											<a class="carousel-control-prev"
												href="#carouselExampleIndicators" role="button"
												data-slide="prev"> <span
												class="carousel-control-prev-icon" aria-hidden="true"></span>
												<span class="sr-only">Previous</span>
											</a> <a class="carousel-control-next"
												href="#carouselExampleIndicators" role="button"
												data-slide="next"> <span
												class="carousel-control-next-icon" aria-hidden="true"></span>
												<span class="sr-only">Next</span>
											</a>
										</div>
									</div>
								</div>
							</div>
							<div class="d-flex">
								<div class="p-2 ml-auto">
									<button type="button" class="btn btn-info">Seleccionar</button>
								</div>
							</div>
						</div>
					</div>

				</div>
			</div>
		</div>
	</div>
</body>
</html>