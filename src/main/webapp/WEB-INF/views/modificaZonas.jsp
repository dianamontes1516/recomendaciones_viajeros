<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
<meta charset="ISO-8859-1">
<title>Zonas</title>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"> </script>
<link rel="stylesheet" href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">

</head>

<body>
<div class="container mt-2">
<div class="card bg-dark text-black">
  <img class="card-img" src="<c:url value="/resources/bacalar.jpg"/>" alt="Card image">
  <div class="card-img-overlay">
	<div class="container mt-2">
		<jsp:include page="HeaderAdministrador.jsp" />
	</div>


<%-- <form:form method="POST" action="atracciones/listar"> --%>

	<p>Zonas en la Base de datos: </p>
	<p>
	
	<form:form method="POST" action="registraZona"  modelAttribute="frmZonaAdmin">

			<table>
				<tr>
	            	<td><form:label path="tipoZona">tipoZona</form:label></td>
	                <td><form:input path="tipoZona"/></td>
	                <td><form:errors path="tipoZona" cssClass="error" /></td>
	             </tr>
	             <tr>
	             	<td colspan="5"><input type="submit" value="Crear Zona"/></td>
	             </tr>
	        </table>
		</form:form> </div>

</body>
</html>