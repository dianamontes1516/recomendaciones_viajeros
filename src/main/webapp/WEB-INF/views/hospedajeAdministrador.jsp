<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isELIgnored="false"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<!DOCTYPE html>
<html>

<head>
<head>
<meta charset="ISO-8859-1">
<title>Hospedajes en Base de datos</title>
<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />"></script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">
<link href="<c:url value="/webjars/font-awesome/css/fontawesome.css"/>"
	rel="stylesheet">
<link href=""
	<c:url value="/webjars/font-awesome/css/brands.css"/>" rel="stylesheet">
<link href="<c:url value="/webjars/font-awesome/css/solid.css"/>"
	rel="stylesheet">
</head>
</head>

<body>


<%--   <img class="card-img" src="<c:url value="/resources/bacalar.jpg"/>" alt="Card image"> --%> 
<!--   <div class="card-img-overlay"> -->
	<div class="container mt-2">
		<jsp:include page="HeaderAdministrador.jsp" />
	</div>

<br>
<br>
<br>

<div class="d-flex bd-highlight">

<div class="p-2 flex-grow-1 bd-highlight">

<p>ACTUALIZA LOS HOSPEDAJES ACTUALES</p>

<div class="accordion" id="accordionExample">
			<div class="card">
				<c:set var="no_opcion" value="1" />
				<c:forEach var="h" items="${formSeleccion.hospedajes}">
					<div class="card-header bg-info text-white"
						id="heading_${no_opcion}">
						<div class="d-flex">
							<div class="m-1">${no_opcion} ${h.nombre}</div>
							<div class="ml-auto">
								<button class="btn btn-link" type="button"
									data-toggle="collapse" data-target="#collapse_${no_opcion}"
									aria-expanded="false" aria-controls="collapse_${no_opcion}">
									<span style="font-size: 1em; color: white;"> <i
										class="fas fa-chevron-circle-down"></i>
									</span>
								</button>
							</div>
						</div>
					</div>

					<div id="collapse_${no_opcion }" class="collapse"
						aria-labelledby="heading_${no_opcion }"
						data-parent="#accordionExample">
						<div class="card-body">
							<div class="container">
								<div class="row justify-content-center">
									<div class="col-md-3">
									<p>${h.nombre}</p>
									<p>${h.latitud}</p>
									<p>${h.longitud}</p> 							
									</div>
																	
								<div class="col-md-1">
							    <form:form method="POST" action="hospedaje-editar" modelAttribute="formSeleccion">							        
							        <form:input path="seleccion" value="${h.hospedajeId}" type="hidden" />
							        <button type="submit" class="btn btn-info"> Editar </button>
							    </form:form>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			<c:set var="no_opcion" value="${no_opcion + 1}" />
			</c:forEach>
			
		</div>
 	</div>
 		
</div>


<div class="p-2 bd-highlight">
<p>CREA UN NUEVO HOSPEDAJE</p>


<a class="btn btn-success" href="/recomendaciones-viajeros/administrador-hospedaje/hospedaje-agrega" role="button">Crear hospedaje</a>

</div>


</div>



</body>
</html>