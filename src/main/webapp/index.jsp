<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>

<script src="<c:url value="/webjars/jquery/jquery.min.js" />"></script>
<script src="<c:url value="/webjars/bootstrap/js/bootstrap.min.js" />">
	
</script>
<link rel="stylesheet"
	href="<c:url value="/webjars/bootstrap/css/bootstrap.min.css" />">


</head>

<body>
	<div class="container mt-2">
		<jsp:include page="WEB-INF/views/Header.jsp" />
		<div class="mt-5 row justify-content-center">
		<div class="col-md-7">
		
			<div id="carouselExampleIndicators" class="carousel slide"
				data-ride="carousel">
				<ol class="carousel-indicators">
					<li data-target="#carouselExampleIndicators" data-slide-to="0"
						class="active"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
					<li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
				</ol>
				<div class="carousel-inner">
					<div class="carousel-item active">
						<img class="d-block w-100"
							src="<c:url value="/resources/bacalar.jpg"/>" alt="First slide">
						<div class="carousel-caption d-none d-md-block">
							<h5>Rel�jate</h5>
							<p>Nosotros encontramos un rinconcito de M�xico para tu
								descanso.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100"
							src="<c:url value="/resources/gastronomia.jpg"/>"
							alt="Second slide">
						<div class="carousel-caption d-none d-md-block">
							<h5>Consi�ntete</h5>
							<p>Nosotros encontramos un rinconcito de M�xico para tu
								descanso.</p>
						</div>
					</div>
					<div class="carousel-item">
						<img class="d-block w-100"
							src="<c:url value="/resources/sierragorda.jpg"/>"
							alt="Third slide">
						<div class="carousel-caption d-none d-md-block">
							<h5>Disfruta</h5>
							<p>Nosotros encontramos un rinconcito de M�xico para tu
								descanso.</p>
						</div>
					</div>
				</div>
				<a class="carousel-control-prev" href="#carouselExampleIndicators"
					role="button" data-slide="prev"> <span
					class="carousel-control-prev-icon" aria-hidden="true"></span> <span
					class="sr-only">Previous</span>
				</a> <a class="carousel-control-next" href="#carouselExampleIndicators"
					role="button" data-slide="next"> <span
					class="carousel-control-next-icon" aria-hidden="true"></span> <span
					class="sr-only">Next</span>
				</a>
			</div>
		
		</div>
		</div>
	</div>

</body>
</html>
