package mx.unam.pa.recomendaciones_viajeros.genetico;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import java.io.*;
import java.util.*;


public class Principal{

    static void creaArchivo(String nombre){
	FileWriter fichero = null;
        PrintWriter pw = null;
        try
        {
            fichero = new FileWriter(nombre);
            pw = new PrintWriter(fichero);

            for (int i = 0; i < 30; i++){
                pw.print("AB-00" + i + ",");
		pw.print(String.valueOf(Math.random() * 100) + ",");
		pw.println(String.valueOf(Math.random() * 100));
	    }

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
           try {
           if (null != fichero)
              fichero.close();
           } catch (Exception e2) {
              e2.printStackTrace();
           }
        }
    }
    
    public static void main(String[] args){

	//creaArchivo("a1.txt");
	//creaArchivo("a2.txt");
	//creaArchivo("a3.txt");
	
	FileReader f;
	BufferedReader b;

	
	String archivos = "src/main/java/mx/unam/pa/recomendaciones_viajeros/genetico/a1.txt,src/main/java/mx/unam/pa/recomendaciones_viajeros/genetico/a2.txt,src/main/java/mx/unam/pa/recomendaciones_viajeros/genetico/a3.txt";
	String linea = "";
	StringTokenizer st = new StringTokenizer(archivos, ",");
	
	StringTokenizer st2; 
	ArrayList<Atraccion> listaAtraccion;

	ArrayList<ArrayList<Atraccion>> aL = new ArrayList<ArrayList<Atraccion>>();
	try{
	    while(st.hasMoreTokens()){
		f = new FileReader(st.nextToken());
		b = new BufferedReader(f);
		listaAtraccion = new ArrayList<Atraccion>();
		while((linea = b.readLine())!=null) {
		    st2 = new StringTokenizer(linea, ",");
		    listaAtraccion.add(new Atraccion(Integer.valueOf(st2.nextToken()), Double.valueOf(st2.nextToken()), Double.valueOf(st2.nextToken())));
		}
		b.close();
		aL.add(listaAtraccion);
	    }
	}catch(IOException e){
	    System.out.println("Error al leer el archivo " + e);
	}

	ConjuntoAtracciones cA = new ConjuntoAtracciones(aL);
	//System.out.println(cA.toString(1));
	//System.out.println("aleatorios " + cA.getAtraccion(1).toString());
	Maestro m = new Maestro(cA, 50, 100);
	ArrayList<Individuo> mejoresIndividuos = m.ejecutaGenetico(5);
	
	for(int i = 0; i < mejoresIndividuos.size(); i++) {
		System.out.println(i +": " + mejoresIndividuos.get(i).toString());
	}
	
	
	
	
    }
   
}
