package mx.unam.pa.recomendaciones_viajeros.genetico;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import java.util.ArrayList;
import java.util.Collections;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.Random;


public class Poblacion{
    public ConjuntoAtracciones conjuntoTotal;
    private ArrayList<Individuo> individuos;
    private int mejor;
    private int peor;

    
    Poblacion(ConjuntoAtracciones conjuntoTotal, int n){
    	this.conjuntoTotal = conjuntoTotal;
    	this.individuos = new ArrayList<Individuo>(n);
    	ArrayList<Atraccion> nuevoIndividuo;
    	for(int i = 0; i < n; i++){
    		nuevoIndividuo = new ArrayList<Atraccion>();
    		for(int j = 0; j < conjuntoTotal.getTiposAtracciones(); j++){
    			nuevoIndividuo.add(conjuntoTotal.getAtraccion(j));
    		}
    		individuos.add(new Individuo(nuevoIndividuo));
    	}
    	this.mejor = -1;
    	this.peor = -1;
    }

    Poblacion(ConjuntoAtracciones conjuntoTotal){
    	this.conjuntoTotal = conjuntoTotal;
    	this.individuos = new ArrayList<Individuo>();
    	this.mejor = -1;
    	this.peor = -1;
    }

    public void insertaIndividuo(Individuo i){
    	this.individuos.add(i);
    }

    public void evaluaPoblacion(){
        ExecutorService executor = Executors.newFixedThreadPool(individuos.size());
        for(Individuo i : individuos){
            executor.execute(i);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }

        mejor = 0;
        peor = 0;
        double mayorAptitud = individuos.get(mejor).getAptitud();
        double peorAptitud = individuos.get(peor).getAptitud();
	
        for(int i = 1; i < individuos.size(); i++){
        	if(individuos.get(i).getAptitud() < mayorAptitud){
        		mayorAptitud = individuos.get(i).getAptitud();
        		mejor = i;
        	}else if(individuos.get(i).getAptitud() > peorAptitud){
        		peorAptitud = individuos.get(i).getAptitud();
        		peor = i;
        	}	
        }
    }


    /**
       Realiza la seleccion de individuos aplicando el metodo de ruleta.
       @return int[] - Regresa el arreglo que contiene los indices de los 
       individuos seleccionados.
    */
    private int[] seleccionRuleta(){
    	double sum = 0.0;
    	int[] indices = new int[individuos.size()];
    	for(Individuo i : individuos){
    		sum += i.getAptitud();
    	}
    	double alpha = 0;
    	double isum = 0;
    	int j = 0;

    	Random r = new Random();
    	r.setSeed(System.currentTimeMillis());
	
    	for(int k = 0; k < individuos.size(); k++){
    		alpha = r.nextDouble() * sum;
    		isum = 0;
    		j = 0;
    		do{
    			isum += individuos.get(j).getAptitud();
    			j++;
    		}while(isum < alpha && j < individuos.size());
    		indices[k] = j-1;
    	}	
    	return indices;
    }

    /**
       Cruza dos individuos y regresa sus hijos (No realiza la evaluacion de los hijos).
       @params p - El individuo uno.
       @params m - El individuo dos.
       @return Un arreglo con los dos Individuos hijos.       
    */
    private Individuo[] cruza(Individuo p, Individuo m){
    	Individuo[] hijos = new Individuo[2];
    	ArrayList<Atraccion> hijo1 = new ArrayList<Atraccion>();
    	ArrayList<Atraccion> hijo2 = new ArrayList<Atraccion>();
    	int punto = (int)(Math.random()*p.getAtracciones().size());
	
    	for(int i = 0; i < p.getAtracciones().size(); i++){
    		if(i < punto){
    			hijo1.add(p.getAtracciones().get(i));
    			hijo2.add(m.getAtracciones().get(i));
    		}
    		else{
    			hijo2.add(p.getAtracciones().get(i));
    			hijo1.add(m.getAtracciones().get(i));
    		}
    	}

    	Individuo i1 = new Individuo(hijo1);
    	Individuo i2 = new Individuo(hijo2);
    	hijos[0] = i1;
    	hijos[1] = i2;
    	return hijos;
    }


    /**
     * Metodo encargado de realizar la cruza de todos los elementos seleccionados 
     para la siguiente generacion.
     * @param indices - Los individuos seleccionados
     * @return La nueva poblacion de individuos que pasara a la siguiente generacion.
     */
    private Poblacion cruza(int[] indices){
    	Poblacion nuevaPoblacion = new Poblacion(this.conjuntoTotal);
    	Individuo[] hijos;

    	for(int i = 0; i < indices.length; i++){
    		hijos = this.cruza(individuos.get(indices[i]), individuos.get(indices[i+1]));
    		i++;
    		nuevaPoblacion.insertaIndividuo(hijos[0]);
    		nuevaPoblacion.insertaIndividuo(hijos[1]);
    	}
    	return nuevaPoblacion;
    }


    public Poblacion cruza(){
    	return this.cruza(this.seleccionRuleta());
    }

    
    public void muta(double probabilidad){
	for(Individuo i: individuos){
	    i.muta(this.conjuntoTotal, probabilidad);
	}
    }

    public void actualizaIndividuo(Individuo mejorIndividuo){
    	individuos.add(peor, mejorIndividuo);
    	individuos.remove(peor+1);
    }


    public Individuo getMejorIndividuo(){
    	return this.individuos.get(mejor);
    }

    public Individuo getPeorIndividuo(){
    	return this.individuos.get(peor);
    }
    
    public ArrayList<Individuo> getIndividuos(){
    	return this.individuos;
    }
    
    public Individuo getIndividuo(int posicion) {
    	return this.individuos.get(posicion);
    }
    
    public String toString(){
    	String cadena="";
    	for(Individuo i : individuos){
    		cadena += i.toString() + "\n";
    	}
    	cadena += "Mejor: " + mejor + "\nPeor: " + peor;
    	return cadena;
    }
    
    public void ordena() {
    	Collections.sort(this.individuos);
    	this.mejor = 0;
    	this.peor = this.individuos.size()-1;
    }

    
}

