package mx.unam.pa.recomendaciones_viajeros.genetico;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;

public class Atracciones{
    private int atraccionId;
    private double latitud;
    private double longitud;

    /**
    @param x la latitud
    @param y la longitud
    */
    Atracciones(int atraccionId, double latitud, double longitud){
    	this.atraccionId = atraccionId;
    	this.latitud = latitud;
    	this.longitud = longitud;
    }

    public static Atracciones copia(Atracciones a){
    	Atracciones nueva = new Atracciones(a.getAtraccionId(), a.getLatitud(), a.getLongitud());
    	return nueva;
    }

	public int getAtraccionId() {
		return this.atraccionId;
	}

    
    public double getLatitud(){
    	return this.latitud;
    }

    
    public double getLongitud(){
    	return this.longitud;
    }

    public String toString(){
    	String cadena = this.atraccionId + "  Lat: " + this.getLatitud() + "  Long:" + this.getLongitud();
    	return cadena;
    } 
}


