package mx.unam.pa.recomendaciones_viajeros.genetico;

import java.util.ArrayList;

public class Maestro{
    private Poblacion poblacion;
    private int generaciones;

    
    public Maestro(ConjuntoAtracciones conjuntoTotal, int n, int generaciones){
    	this.poblacion = new Poblacion(conjuntoTotal, n);
    	this.generaciones = generaciones;
    }

    public void ejecutaGenetico(){
    	this.poblacion.evaluaPoblacion();
    	//System.out.println(this.toString());
	
    	Poblacion nueva;

    	for(int j = 0; j < generaciones; j++){
    		nueva = this.poblacion.cruza();
    		nueva.muta(0.3);
    		nueva.evaluaPoblacion();
    		nueva.actualizaIndividuo(this.poblacion.getMejorIndividuo());
    		this.poblacion = nueva;
    	}
    	poblacion.evaluaPoblacion();
    }
    
    public ArrayList<Individuo> ejecutaGenetico(int no_mejores){
    	this.poblacion.evaluaPoblacion();
    	ArrayList<Individuo> mejores = new ArrayList<Individuo>();
    	//System.out.println(this.toString());
	
    	Poblacion nueva;

    	for(int j = 0; j < generaciones; j++){
    		nueva = this.poblacion.cruza();
    		nueva.muta(0.3);
    		nueva.evaluaPoblacion();
    		nueva.actualizaIndividuo(this.poblacion.getMejorIndividuo());
    		this.poblacion = nueva;
    	}
    	poblacion.evaluaPoblacion();
    	
    	poblacion.ordena();
    	
    	if(no_mejores > this.poblacion.getIndividuos().size())
    		return this.poblacion.getIndividuos();
    	
    	Individuo ultimo = this.poblacion.getIndividuo(0);
    	mejores.add(ultimo);
    	Individuo actual = null;
    	int i = 1;
    	int k = 1;
    	while(i < no_mejores && k < this.poblacion.getIndividuos().size()) {
    		actual = this.poblacion.getIndividuo(k);
    		if(!ultimo.equals(actual)) {
    			mejores.add(this.poblacion.getIndividuo(k));
    			ultimo = actual;
    			i++;
    			k++;
    		}
    		else {
    			k++; 
    		}
    	}
    	
    	return mejores;
    }
    
    
    
    
    public Poblacion getPoblacion() {
    	return this.poblacion;
    }
    
    public int getGeneraciones() {
    	return this.generaciones;
    }
    
    
    public String toString(){
    	String cadena = poblacion.toString();
    	cadena += "Mejor " + poblacion.getMejorIndividuo().toString() + "\n";
    	cadena += "Peor " + poblacion.getPeorIndividuo().toString();
    	return cadena;
    }

    
}

