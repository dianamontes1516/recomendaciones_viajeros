package mx.unam.pa.recomendaciones_viajeros.genetico;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import java.util.ArrayList;

public class ConjuntoAtracciones{
    private ArrayList<ArrayList<Atraccion>> atracciones;

    public ConjuntoAtracciones(ArrayList<ArrayList<Atraccion>> atracciones){
    	this.atracciones = atracciones;
    }

    /**
       Regresa una atraccion aleatoria de la lista x
       @param x la posicion de la lista
     */
    public Atraccion getAtraccion(int x){
    	int posicion = (int)(Math.random() * atracciones.get(x).size());
    	return (atracciones.get(x)).get(posicion);
    }

    public int getTiposAtracciones(){
    	return atracciones.size();
    }

    public String toString(int x){
    	String cadena="";
    	ArrayList<Atraccion> lista = atracciones.get(x);
    	for(Atraccion a : lista){
    		cadena += a.toString() + "\n";
    	}
	return cadena;
    } 
}
