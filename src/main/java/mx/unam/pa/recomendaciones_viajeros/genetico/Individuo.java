package mx.unam.pa.recomendaciones_viajeros.genetico;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import java.util.ArrayList;

public class Individuo extends Thread implements Comparable {
	private ArrayList<Atraccion> atracciones;
	private double aptitud;

	public Individuo(ArrayList<Atraccion> atracciones) {
		this.atracciones = new ArrayList<Atraccion>(atracciones.size());
		this.aptitud = 0;

		for (Atraccion a : atracciones) {
			this.atracciones.add(Atraccion.copia(a));
		}
	}

	public void calculaAptitud() {
		double minLat, maxLat, minLon, maxLon, temp;
		minLat = atracciones.get(0).getLatitud();
		minLon = atracciones.get(0).getLongitud();
		maxLat = minLat;
		maxLon = minLon;

		for (Atraccion a : atracciones) {
			temp = a.getLatitud();
			if (temp < minLat)
				minLat = temp;
			else if (temp > maxLat)
				maxLat = temp;
			temp = a.getLongitud();
			if (temp < minLon)
				minLon = temp;
			else if (temp > maxLon)
				maxLon = temp;
		}

		// aptitud = (maxX - minX) * (maxY - minY);
		this.aptitud = DistanceCalculator.distance(minLat, minLon, maxLat, maxLon, "K");
	}

	public double[] calculaRectangulo() {
		double minLat, maxLat, minLon, maxLon, temp;
		minLat = atracciones.get(0).getLatitud();
		minLon = atracciones.get(0).getLongitud();
		maxLat = minLat;
		maxLon = minLon;

		for (Atraccion a : atracciones) {
			temp = a.getLatitud();
			if (temp < minLat)
				minLat = temp;
			else if (temp > maxLat)
				maxLat = temp;
			temp = a.getLongitud();
			if (temp < minLon)
				minLon = temp;
			else if (temp > maxLon)
				maxLon = temp;
		}

		double[] array = { minLat, maxLat, minLon, maxLon };
		return array;
	}

	public void muta(ConjuntoAtracciones ca, double probabilidad) {
		double aleatorio = Math.random();
		if (aleatorio >= probabilidad) {
			int p = (int) (Math.random() * atracciones.size());
			Atraccion nueva = ca.getAtraccion(p);
			atracciones.add(p, nueva);
			atracciones.remove(p + 1);
		}
	}

	@Override
	public void run() {
		this.calculaAptitud();
	}

	public ArrayList<Atraccion> getAtracciones() {
		return atracciones;
	}

	public double getAptitud() {
		return aptitud;
	}

	public void setAptitud(double apt) {
		this.aptitud = apt;
	}

	public String toString() {
		String cadena = "Aptitud = " + aptitud + "\n";
		for (Atraccion a : atracciones) {
			cadena += a.toString() + "\n";
		}

		return cadena;
	}

	@Override
	public int compareTo(Object arg0) {
		double apt = ((Individuo) arg0).getAptitud();
		return (int) (this.aptitud - apt);
	}

	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true; 

		if (!(o instanceof Individuo))
			return false;

		Individuo c = (Individuo) o;

		if (c.getAtracciones().size() != this.atracciones.size())
			return false;

		ArrayList<Atraccion> aObjeto = c.getAtracciones();

		for (int i = 0; i < this.atracciones.size(); i++) {
			if (!this.atracciones.get(i).equals(aObjeto.get(i)))
				return false;
		}
		return true;
	}
	
	public String idsAtrString() {
		int longitud = this.atracciones.size();
		String s = Integer.toString(this.atracciones.get(0).getAtraccionId());
		for(int i = 1; i < longitud; i++) {
			s += ","+Integer.toString(this.atracciones.get(i).getAtraccionId());
		}
		return s;
	}
}
