package mx.unam.pa.recomendaciones_viajeros.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;


@Controller
public class AdministradorSalirController {

	@RequestMapping("/logout")
	public String cerrarSesionUsuario(HttpSession session) {
		System.out.println("Cerrando sesión de administrador");
		session.invalidate();
		
		System.out.println("Redireccionando a la pantalla de inicio");
		return "redirect:/";
	}
}