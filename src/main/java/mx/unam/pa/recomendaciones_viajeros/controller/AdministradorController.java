package mx.unam.pa.recomendaciones_viajeros.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
 
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import mx.unam.pa.recomendaciones_viajeros.service.AdministradorService;
import mx.unam.pa.recomendaciones_viajeros.service.AtraccionService;
import mx.unam.pa.recomendaciones_viajeros.service.TipoHospedajeService;
import mx.unam.pa.recomendaciones_viajeros.service.TipoZonaService;
import mx.unam.pa.recomendaciones_viajeros.formas.AdminLoginFrm;
import mx.unam.pa.recomendaciones_viajeros.formas.AdminZonaFrm;
import mx.unam.pa.recomendaciones_viajeros.model.Administrador;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;

@Controller
@SessionAttributes("usuarioFirmado")
public class AdministradorController {

	@Autowired
	private AdministradorService servicio;
	
	@Autowired
	private AtraccionService atraccionServicio;
	
	@Autowired
	private TipoHospedajeService tipoHospedajeServicio;
	
	@Autowired
	private TipoZonaService tipoZonaServicio;

	@GetMapping("/admin")
	public ModelAndView inicioAdmin() {
		return new ModelAndView("principal-administrador");
	}
	
	@GetMapping("/login")
	public ModelAndView mostrarFormaLogin() {
		System.out.println("Mostrando forma de login");
		return new ModelAndView("inicio-administrador", // Nombre de la vista (nombre del JSP sin extensión)
				"frmUsuarioAdmin", // nombre del modelo. Usado en la página que inyectará los campos de la forma 
				new AdminLoginFrm() // Objeto del modelo que se asociará a la vista
			);
		
	}
	
	@RequestMapping( value = "/inicioSesion", method = RequestMethod.POST )
	public ModelAndView loginUsuario(@Valid @ModelAttribute("frmUsuarioAdmin") AdminLoginFrm frmLogin,
			BindingResult resultado, // Resultado de la validación 
			ModelAndView view //  modelo a regresar
		) {
		
		//System.out.println("Dentro " + frmLogin.getUsuario() + frmLogin.getContraseña());
		if( resultado.hasErrors() ) {
			System.err.println("La validación de la forma presentó errores");
			view.setViewName("inicio-administrador");
			return view;
		}
		
		System.out.println("Buscando al usuario en la DB con el criterio: " + frmLogin);
		
		//Administrador usuarioFirmado = servicio.login(frmLogin);
		Administrador usuarioFirmado = servicio.login(frmLogin);
		
		if( usuarioFirmado != null ) { // autenticación correcta
			System.out.println("Usuario encontrado en la DB: " + usuarioFirmado);
			view.addObject("usuarioFirmado", usuarioFirmado); // Creación de la sesión de usuario
			view.setViewName("principal-administrador");
		}else {
			System.out.println("Usuario no encontrado en la DB");
			view.addObject("error", "Nombre de usuario y/o contraseña incorrectos");
			view.setViewName("inicio-administrador");
		}
		return view;
	}
	
	@RequestMapping( value = "/hoteles/listar")
	public ModelAndView listarHoteles() {			
		ModelAndView view = new ModelAndView();
		
		List<TipoHospedaje> tipoHospedajes = tipoHospedajeServicio.getAll();
		
		view.addObject("tipoHospedajes", tipoHospedajes);
		view.setViewName("hospedajeAdministrador");
		return view;
	}
	
	@RequestMapping( value = "/zonas/listar")
	public ModelAndView listarZonas() {
		ModelAndView view = new ModelAndView();
		
		List<TipoZona> tipoZonas = tipoZonaServicio.getAll();
			
		view.addObject("tipoZonas", tipoZonas);
		view.setViewName("zonasAdministrador");
		
		return view;
	}

}
