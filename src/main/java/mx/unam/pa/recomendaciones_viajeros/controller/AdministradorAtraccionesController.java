package mx.unam.pa.recomendaciones_viajeros.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.ModelAndView;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.service.AtraccionService;

@Controller
@SessionAttributes("usuarioFirmado")
//@RequestMapping("/atracciones")
public class AdministradorAtraccionesController {
	
	@Autowired
	private AtraccionService atraccionServicio; 
	
	/**
	 * Lista el conjunto de las atracciones para el administrador
	 * @return
	 */
	@GetMapping("/atracc/listar")
	public ModelAndView listar() {
		ModelAndView view = new ModelAndView();
		
		List<Atraccion> atracciones = atraccionServicio.getAll();
		
		view.addObject("atracciones", atracciones);
		view.setViewName("atraccionesAdministrador");
		return view;
	}
}
