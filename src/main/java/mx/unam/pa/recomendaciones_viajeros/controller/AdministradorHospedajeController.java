package mx.unam.pa.recomendaciones_viajeros.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import mx.unam.pa.recomendaciones_viajeros.formas.AdminLoginFrm;
import mx.unam.pa.recomendaciones_viajeros.formas.HospedajeFrm;
import mx.unam.pa.recomendaciones_viajeros.formas.SeleccionHospedajeFrm;
import mx.unam.pa.recomendaciones_viajeros.forms.SeleccionTipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Administrador;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;
import mx.unam.pa.recomendaciones_viajeros.service.HospedajeService;


@Controller
@RequestMapping("/administrador-hospedaje")
public class AdministradorHospedajeController {

		@Autowired
		private HospedajeService hServicio;
	
		@GetMapping("/hospedajes")
		public ModelAndView administraHospedajes() {
			ModelAndView view = new ModelAndView();

			List<Hospedaje> catalogoHospedajes = hServicio.getAll();
			SeleccionHospedajeFrm formSeleccion = new SeleccionHospedajeFrm();
			formSeleccion.setHospedajes(catalogoHospedajes);
			
			view.addObject("formSeleccion", formSeleccion);
			view.setViewName("hospedajeAdministrador");
			return view;
		}
		
		@RequestMapping(value = "/hospedaje-editar", method = RequestMethod.POST)
		public ModelAndView editarHospedaje(@ModelAttribute("formSeleccion") SeleccionHospedajeFrm formSeleccion,
				ModelAndView view ) {
							
				Hospedaje h = hServicio.getById(formSeleccion.seleccion);
				System.out.println("hospedaje elegido " + formSeleccion);
				view.addObject("hospedajeSeleccion", h);
				view.addObject("frmHospedaje",new HospedajeFrm());
				view.setViewName("hospedaje-edita");
				return view;
		}
		
		
		@RequestMapping( value = "/actualiza-hospedaje", method = RequestMethod.POST )
		public ModelAndView actualizaHospedaje(@Valid @ModelAttribute("frmHospedaje") HospedajeFrm frmHospedaje,
				BindingResult resultado, // Resultado de la validación 
				ModelAndView view //  modelo a regresar
			) {
			
			System.out.println("Dentro " + frmHospedaje);
			if( resultado.hasErrors() ) {
				System.err.println("La validación de la forma presentó errores");
				view.setViewName("hospedaje-edita");
				return view;
			}
			
			System.out.println("Buscando al usuario en la DB con el criterio: " + frmHospedaje.getNombre());
			
			Hospedaje h = new Hospedaje(frmHospedaje.getSeleccion(), frmHospedaje.getNombre(), 
					(double)frmHospedaje.getLatitud(),(double)frmHospedaje.getLongitud(), (int)frmHospedaje.getCosto());
			System.out.println("hospedaje creado " + h);
			hServicio.updateOne(h);
			
			List<Hospedaje> catalogoHospedajes = hServicio.getAll();
			SeleccionHospedajeFrm formSeleccion = new SeleccionHospedajeFrm();
			formSeleccion.setHospedajes(catalogoHospedajes);
			
			view.addObject("formSeleccion", formSeleccion);
			view.setViewName("hospedajeAdministrador");
			return view;
		}
		
		
		@RequestMapping( value = "/hospedaje-agrega")
		public ModelAndView agregaHospedaje(@Valid @ModelAttribute("frmHospedaje") HospedajeFrm frmHospedaje,
				BindingResult resultado, // Resultado de la validación 
				ModelAndView view //  modelo a regresar
				) {
			
			System.out.println("Dentro de agregar " + frmHospedaje);
			if( resultado.hasErrors() ) {
				System.err.println("La validación de la forma presentó errores");
				view.setViewName("hospedaje-agregar");
				return view;
			}
			
			hServicio.createWhitValues(frmHospedaje.getNombre(), frmHospedaje.getLatitud(), 
					frmHospedaje.getLongitud(), (int)frmHospedaje.getCosto());
			
			List<Hospedaje> catalogoHospedajes = hServicio.getAll();
			SeleccionHospedajeFrm formSeleccion = new SeleccionHospedajeFrm();
			formSeleccion.setHospedajes(catalogoHospedajes);
			
			view.addObject("formSeleccion", formSeleccion);
			view.setViewName("hospedajeAdministrador");
			return view;
			
		}
		
		
		@RequestMapping( value = "/elimina-hospedaje", method = RequestMethod.POST )
		public ModelAndView eliminaHospedaje(@Valid @ModelAttribute("frmHospedaje") HospedajeFrm frmHospedaje,
				BindingResult resultado, // Resultado de la validación 
				ModelAndView view //  modelo a regresar
			) {
			
			Hospedaje h = hServicio.getById(frmHospedaje.getSeleccion());
			System.out.println("hospedaje a eliminar " + h);
			hServicio.eliminateOne(h);
			
			List<Hospedaje> catalogoHospedajes = hServicio.getAll();
			SeleccionHospedajeFrm formSeleccion = new SeleccionHospedajeFrm();
			formSeleccion.setHospedajes(catalogoHospedajes);
			
			view.addObject("formSeleccion", formSeleccion);
			view.setViewName("hospedajeAdministrador");
			return view;
			
		}
		
}
