package mx.unam.pa.recomendaciones_viajeros.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mx.unam.pa.recomendaciones_viajeros.forms.AtraccionFom;
import mx.unam.pa.recomendaciones_viajeros.forms.SeleccionIndividuo;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;
import mx.unam.pa.recomendaciones_viajeros.service.AtraccionService;
import mx.unam.pa.recomendaciones_viajeros.service.TipoAtraccionService;

@Controller
@RequestMapping("/atraccion")
public class AtraccionController {

	@Autowired
	private AtraccionService aServicio;

	@Autowired
	private TipoAtraccionService taServicio;

	@GetMapping("/nueva")
	public ModelAndView nueva() {
		
		ModelMap model = new ModelMap();
		
		model.addAttribute("catalogoTipos", taServicio.getAll());
		model.addAttribute("atraccion", new AtraccionFom());
	
		ModelAndView view = new ModelAndView("nueva-atraccion", model);
		
		return view;
	}
	
	@RequestMapping(value = "/agrega-atraccion", method = RequestMethod.POST)
	public ModelAndView recomendacionHospedaje(
			@Valid @ModelAttribute("atraccion") AtraccionFom atraccion, 
			BindingResult resultado,
			ModelAndView view 
		) {
		if( resultado.hasErrors() ) {
			ModelMap model = new ModelMap();
			
			model.addAttribute("catalogoTipos", taServicio.getAll());
			model.addAttribute("atraccion", atraccion);
			model.addAttribute("resultado", resultado);

			System.err.println("La validación de la forma presentó errores");
			view.setViewName("nueva-atraccion");
			view.addAllObjects(model);
			return view;
		} 
		aServicio.saveF(atraccion);			
		
		return listaAtracciones();
	}
	
	@GetMapping("/lista")
	public ModelAndView listaAtracciones() {
		ModelMap model = new ModelMap();
		model.addAttribute("atracciones", aServicio.getAll());
		model.addAttribute("edicion", new AtraccionFom());
		model.addAttribute("catalogoTipos", taServicio.getAll());
		
		ModelAndView view = new ModelAndView("lista-atracciones", model);
		return view;
	}
	
	@GetMapping(value = "/elimina/{id}")
	public ModelAndView elimina(@PathVariable Integer id) {
		Atraccion atraccion = new Atraccion(id);
		
		aServicio.eliminateOne(atraccion);
		List<Atraccion> atracciones = aServicio.getAll();
		return listaAtracciones();		
	}

	@RequestMapping(value = "/edita", method = RequestMethod.POST)
	public ModelAndView edita(@Valid @ModelAttribute("seleccion") Atraccion atraccion, 
			ModelAndView view 
		) {
		aServicio.updateOne(atraccion);
		
		return listaAtracciones();
	}

}


