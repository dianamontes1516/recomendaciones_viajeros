package mx.unam.pa.recomendaciones_viajeros.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import mx.unam.pa.recomendaciones_viajeros.forms.SeleccionIndividuo;
import mx.unam.pa.recomendaciones_viajeros.forms.SeleccionTipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.service.AtraccionService;
import mx.unam.pa.recomendaciones_viajeros.service.GeneticoService;
import mx.unam.pa.recomendaciones_viajeros.service.HospedajeService;
import mx.unam.pa.recomendaciones_viajeros.service.TipoAtraccionService;

@Controller
@RequestMapping("/recomendacion")
public class RecomendacionesController {

	@Autowired
	private TipoAtraccionService servicio;

	@Autowired
	private AtraccionService aServicio;
	
	@Autowired
	private GeneticoService gServicio;
	
	@Autowired
	private HospedajeService hServicio;

	@GetMapping("/nueva")
	public ModelAndView nuevaRecomendacion() {
		ModelAndView view = new ModelAndView();

		List<TipoAtraccion> catalogoTipos = servicio.getAll();
		SeleccionTipoAtraccion formSeleccion = new SeleccionTipoAtraccion();
		formSeleccion.setCatalogo(catalogoTipos);
		
		view.addObject("formSeleccion", formSeleccion);
		view.setViewName("nueva-consulta");
		return view;
	}

	@RequestMapping(value = "/consulta", method = RequestMethod.POST)
	public ModelAndView recibeSeleccionTipos(
				@Valid @ModelAttribute("formSeleccion") SeleccionTipoAtraccion formSeleccion, 
				ModelAndView view 
			) {
		
		ArrayList<TipoAtraccion> tipos = new ArrayList();
		for (String s: formSeleccion.seleccionTipos) {
			TipoAtraccion t = new TipoAtraccion();
			t.setTipoAtraccionId(Integer.parseInt(s));
			tipos.add(t);
		}
		
		ArrayList<Individuo> recomendaciones = gServicio.getArrayMejoresAtracciones(tipos, 5);

		System.out.println(recomendaciones);
		SeleccionIndividuo si = new SeleccionIndividuo(recomendaciones);
		
		view.addObject("formSeleccionIndividuo", si);
		view.setViewName("resultados-consulta");
		return view;
	}
	
	@RequestMapping(value = "/hospedaje", method = RequestMethod.POST)
	public ModelAndView recomendacionHospedaje(
			@Valid @ModelAttribute("formSeleccionIndividuo") SeleccionIndividuo formSeleccion, 
			ModelAndView view 
		) {
		
		ArrayList<Atraccion> atracciones = new ArrayList();
		for (Integer i: formSeleccion.seleccion) {
			atracciones.add(aServicio.getById(i));
		}
		System.out.println(atracciones);
		
		Individuo individuo = new Individuo(atracciones);
		individuo.calculaAptitud();
		List<Hospedaje> hospedajes = hServicio.getAllBetween(individuo);
		
		System.out.println(hospedajes);
			
		view.addObject("hospedajes", hospedajes);
		view.setViewName("resultados-hospedaje");
		
		return view;
	}
	
}
