package mx.unam.pa.recomendaciones_viajeros.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Table;
//import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

/**
 * Mapeo de un POJO con una tabla a través
 * de anotaciones a través de la especificación
 * estándar de JPA.
 *
 * */
@Entity
@Table(name = "administrador")
public class Administrador {

	@Id
	@Column(name = "persona_id")
	private int persona_id;
	
	//@OneToOne(mappedBy = "administrador", cascade = CascadeType.ALL, fetch = FetchType.LAZY, optional = false)
	//private Persona persona;
	
	/*@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "persona_id", referencedColumnName = "persona_id")
	private Persona persona;*/

	@Column(name = "usuario")
	private String usuario;

	@Column(name = "contraseña")
	private String contraseña;

	/**
	 * Constructor sin argumentos requerido
	 */
	public Administrador() {}

	/**
	 * Constructor completo
	 * @param usuario
	 * @param contraseña
	 * @param persona
	 */
	public Administrador(String usuario, String contraseña, Persona persona) {
		this.usuario = usuario;
		this.contraseña = contraseña;
		//this.persona = persona;
	}

	public Administrador(String usuario, String contraseña) {
		this.usuario = usuario;
		this.contraseña = contraseña;
		//this.persona = null;
	}
	
	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getContraseña() {
		return contraseña;
	}

	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}

	/*public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}*/

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Administrador [usuario=").append(usuario).append(",contraseña=").append(contraseña).append("]");
		return builder.toString();
	}

}
