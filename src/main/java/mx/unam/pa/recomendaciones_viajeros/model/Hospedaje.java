package mx.unam.pa.recomendaciones_viajeros.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Mapeo de un POJO con una tabla a traves
 * de anotaciones estandar de JPA.
 *
 * */
@Entity
@Table(name = "hospedaje")
public class Hospedaje {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="hospedaje_id")
	private int hospedajeId;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "latitud")
	private double latitud;

	@Column(name = "longitud")
	private double longitud;

	@Column(name = "costo")
	private int costo;

	@ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(
    		name="hospedaje_tipos",
    		joinColumns = { @JoinColumn(name = "hospedaje_id") },
    		inverseJoinColumns = @JoinColumn(name= "tipo_hospedaje_id")
    		)
    private Set<TipoHospedaje> relacionTipoHospedaje = new HashSet<>();

	/**
	 * Constructor sin argumentos requerido
	 */
	public Hospedaje() {}

	/**
	 * Constructor completo
	 * @param hospedajeId
	 * @param nombre
	 * @param latitud
	 * @param longitud
	 * @param costo
	 */
	public Hospedaje(int hospedajeId, String nombre, double latitud, double longitud, int costo) {
		this.hospedajeId = hospedajeId;
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = costo;
	}

	/**
	 * Constructor simple
	 * @param hospedajeId
	 * @param latitud
	 * @param longitud
	 */
	public Hospedaje(int hospedajeId, double latitud, double longitud) {
		this.hospedajeId = hospedajeId;
		this.nombre = "";
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = 0;
	}

	/**
	 * Constructor sin id
	 * @param nombre
	 * @param latitud
	 * @param longitud
	 * @param costo
	 */
	public Hospedaje(String nombre, double latitud, double longitud, int costo) {
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = costo;
	}

	/**
	 * getters and setters
	 * @return
	 */
	public int getHospedajeId() {
		return this.hospedajeId;
	}

	public String getNombre() {
		return this.nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getLatitud() {
		return this.latitud;
	}

	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}

	public double getLongitud() {
		return this.longitud;
	}

	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}

	public int getCosto() {
		return this.costo;
	}

	public void setCosto(int costo) {
		this.costo = costo;
	}

	public Set<TipoHospedaje> getRelacionTipoHospedaje(){
		return this.relacionTipoHospedaje;
	}

	@Override
	public String toString() {
		return "Hospedaje [hospedajeId=" + hospedajeId + ", nombre=" + nombre + ", latitud=" + latitud + ", longitud="
				+ longitud + ", costo=" + costo + ", relacionTipoHospedaje=" + relacionTipoHospedaje + "]";
	}

}
