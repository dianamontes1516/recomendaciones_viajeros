package mx.unam.pa.recomendaciones_viajeros.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Mapeo de un POJO con una tabla a traves 
 * de anotaciones estandar de JPA.
 *
 * */
@Entity
@Table(name = "tipo_atraccion")
public class TipoAtraccion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tipo_atraccion_id")
	private int tipoAtraccionId;
	
	@Column(name = "tipo_atraccion")
	private String tipoAtraccion;

	/**
	 * Constructor sin argumentos requerido
	 */
	public TipoAtraccion() {}
	
	/**
	 * Constructor completo
	 * @param tipoAtraccionId
	 * @param tipoAtraccion
	 */
	public TipoAtraccion(int tipoAtraccionId, String tipoAtraccion) {
		this.tipoAtraccionId = tipoAtraccionId;
		this.tipoAtraccion = tipoAtraccion;
	}
	
	/**
	 * Constructor sin id
	 * @param tipoAtraccion
	 */
	public TipoAtraccion(String tipoAtraccion) {
		this.tipoAtraccion = tipoAtraccion;
	}

	/**
	 * getters and setters
	 * @return
	 */

	public int getTipoAtraccionId() {
		return this.tipoAtraccionId;
	}
	
	public void setTipoAtraccionId(int tipoAtraccionId) {
		this.tipoAtraccionId = tipoAtraccionId;
	}

	public String getTipoAtraccion() {
		return this.tipoAtraccion;
	}
	
	public void setTipoAtraccion(String tipoAtraccion) {
		this.tipoAtraccion = tipoAtraccion;
	}

	@Override
	public String toString() {
		return "TipoAtraccion [tipoAtraccionId=" + tipoAtraccionId + ", tipoAtraccion=" + tipoAtraccion + "]";
	}
	
}
