package mx.unam.pa.recomendaciones_viajeros.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;

/**
 * Mapeo de un POJO con una tabla a traves de anotaciones estandar de JPA.
 *
 */
@Entity
@Table(name = "atraccion")
public class Atraccion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "atraccion_id")
	private Integer atraccionId;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "latitud")
	private Double latitud;

	@Column(name = "longitud")
	private Double longitud;

	@Column(name = "costo")
	private Integer costo;

	@Column(name = "tipo_atraccion_id")
	private Integer tipoAtraccionId;

	@ManyToOne
	@JoinColumn(name = "tipo_atraccion_id", nullable = false, insertable = false, updatable = false)
	private TipoAtraccion tipoAtraccion;

//	@ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(
//    		name="atraccion_zona",
//    		joinColumns = { @JoinColumn(name = "atraccion_id") },
//    		inverseJoinColumns = @JoinColumn(name= "tipo_zona_id")
//    		)
//    private Set<TipoZona> relacionTipoZona = new HashSet<>();

	/**
	 * Constructor sin argumentos requerido
	 */
	public Atraccion() {
	}

	public Atraccion(Integer id) {
		this.atraccionId = id;
	}

	/**
	 * Constructor completo
	 * 
	 * @param atraccionId
	 * @param nombre
	 * @param latitud
	 * @param longitud
	 * @param costo
	 * @param tipoAtraccionId
	 */
	public Atraccion(Integer atraccionId, String nombre, Double latitud, Double longitud, Integer costo,
			int tipoAtraccionId) {
		this.atraccionId = atraccionId;
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = costo;
		this.tipoAtraccionId = tipoAtraccionId;
	}

	/**
	 * Constructor simple
	 * 
	 * @param atraccionId
	 * @param longitud
	 * @param latitud
	 */
	public Atraccion(Integer atraccionId, Double latitud, Double longitud) {
		this.atraccionId = atraccionId;
		this.nombre = "";
		this.longitud = longitud;
		this.latitud = latitud;
		this.costo = 0;
		this.tipoAtraccionId = 1;
	}

	/**
	 * Constructor sin id
	 * 
	 * @param nombre
	 * @param longitud
	 * @param latitud
	 * @param costo
	 * @param tipoAtraccionId
	 */
	public Atraccion(String nombre, double latitud, double longitud, int costo, int tipoAtraccionId) {
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = costo;
		this.tipoAtraccionId = tipoAtraccionId;
	}

	public void setAtraccionId(Integer atraccionId) {
		this.atraccionId = atraccionId;
	}


//	public Set<TipoZona> getRelacionTipoZona() {
//		return this.relacionTipoZona;
//	}
//
//	public void setRelacionTipoZona(Set<TipoZona> relacionTipoZona) {
//		this.relacionTipoZona = relacionTipoZona;
//	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public Integer getCosto() {
		return costo;
	}

	public void setCosto(Integer costo) {
		this.costo = costo;
	}

	public Integer getTipoAtraccionId() {
		return tipoAtraccionId;
	}

	public void setTipoAtraccionId(Integer tipoAtraccionId) {
		this.tipoAtraccionId = tipoAtraccionId;
	}

	public TipoAtraccion getTipoAtraccion() {
		return tipoAtraccion;
	}

	public void setTipoAtraccion(TipoAtraccion tipoAtraccion) {
		this.tipoAtraccion = tipoAtraccion;
	}

	public Integer getAtraccionId() {
		return atraccionId;
	}

	public String getNombreTipoAtraccion() {
		return this.tipoAtraccion.getTipoAtraccion();
	}

	public static Atraccion copia(Atraccion a) {
		Atraccion nueva = new Atraccion(a.getAtraccionId(), a.getNombre(), a.getLatitud(), a.getLongitud(),
				a.getCosto(), a.getTipoAtraccionId());
		return nueva;
	}

	/**
	 * Utilizado en el genético
	 */
	@Override
	public boolean equals(Object o) {
		if (o == this)
			return true;

		if (!(o instanceof Atraccion))
			return false;

		Atraccion c = (Atraccion) o;

		return (Integer.compare(this.atraccionId, c.getAtraccionId()) == 0)
				&& (Double.compare(this.latitud, c.getLatitud()) == 0)
				&& (Double.compare(this.longitud, c.getLongitud()) == 0);
	}

	@Override
	public String toString() {
		return "Atraccion [atraccionId=" + atraccionId + ", nombre=" + nombre + ", latitud=" + latitud + ", longitud="
				+ longitud + ", costo=" + costo + ", tipoAtraccionId=" + tipoAtraccionId + ", tipoAtraccion="
				+ tipoAtraccion + "]";
	}
	// + ",relacionTipoZona=" + relacionTipoZona

}
