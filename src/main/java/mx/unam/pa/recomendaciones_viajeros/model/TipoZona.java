package mx.unam.pa.recomendaciones_viajeros.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tipo_zona")
public class TipoZona {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="tipo_zona_id")
	private int tipoZonaId;

	@Column(name = "tipo_zona")
	private String tipoZona;

//	@ManyToMany(mappedBy = "relacionTipoZona")
//	private Set<Atraccion> relacionAtracciones = new HashSet<>();

	/**
	 * Constructor sin argumentos requerido
	 */
	public TipoZona() {}

	/**
	 * Constructor completo
	 * @param tipoZonaId
	 * @param tipoZona
	 */
	public TipoZona(int tipoZonaId, String tipoZona) {
		this.tipoZonaId = tipoZonaId;
		this.tipoZona = tipoZona;
	}

	/**
	 * Constructor sin id
	 * @param tipoZonaId
	 * @param tipoZona
	 */
	public TipoZona(String tipoZona) {
		this.tipoZona = tipoZona;
	}

	/**
	 * getters and setters
	 * @return
	 */
	public int getTipoZonaId() {
		return this.tipoZonaId;
	}

	public String getTipoZona() {
		return this.tipoZona;
	}

	public void setTipoZona(String tipoZona) {
		this.tipoZona = tipoZona;
	}

	@Override
	public String toString() {
		return "TipoZona [tipoZonaId=" + tipoZonaId + ", tipoZona=" + tipoZona + "]";
	}

}
