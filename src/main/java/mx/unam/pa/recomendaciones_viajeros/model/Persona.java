package mx.unam.pa.recomendaciones_viajeros.model;

//import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
//import javax.persistence.JoinColumn;
import javax.persistence.MapsId;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "persona")
public class Persona {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="persona_id")
	private int personaId;

	@Column(name = "nombre")
	private String nombre;

	@Column(name = "apellido_paterno")
	private String apPaterno;

	@Column(name = "apellido_materno")
	private String apMaterno;

	@Column(name = "correo")
	private String correo;

	@OneToOne(fetch = FetchType.LAZY)
    @MapsId
    private Administrador administrador;

	/**
	 * Constructor sin argumentos requerido
	 */
	public Persona() {}

	public Persona(int personaId, String nombre, String apPaterno, String apMaterno, String correo) {
		this.personaId = personaId;
		this.correo = correo;
		this.nombre = nombre;
		this.apPaterno = apPaterno;
		this.apMaterno = apMaterno;
	}

	public Persona(String nombre, String apPaterno, String apMaterno, String correo) {
		this.correo = correo;
		this.nombre = nombre;
		this.apPaterno = apPaterno;
		this.apMaterno = apMaterno;
	}

	public int getPersonaId() {
		return personaId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApPaterno() {
		return apPaterno;
	}

	public void setApPaterno(String apPaterno) {
		this.apPaterno = apPaterno;
	}

	public String getApMaterno() {
		return apMaterno;
	}

	public void setApMaterno(String apMaterno) {
		this.apMaterno = apMaterno;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	@Override
	public String toString() {
		return "Persona [personaId=" + personaId + ", nombre=" + nombre + ", apPaterno=" + apPaterno + ", apMaterno="
				+ apMaterno + ", correo=" + correo + "]";
	}


}
