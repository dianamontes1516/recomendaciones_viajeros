package mx.unam.pa.recomendaciones_viajeros.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

/**
 * Mapeo de un POJO con una tabla a traves 
 * de anotaciones estandar de JPA.
 *
 * */
@Entity
@Table(name = "tipo_hospedaje")
public class TipoHospedaje {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="tipo_hospedaje_id")
	private int tipoHospedajeId;
	
	@Column(name = "tipo_hospedaje")
	private String tipoHospedaje;
			
	@ManyToMany(mappedBy = "relacionTipoHospedaje")
	private Set<Hospedaje> relacionHospedajes = new HashSet<>();

	/**
	 * Constructor sin argumentos requerido
	 */
	public TipoHospedaje() {}
	
	/**
	 * Constructor completo
	 * @param tipoZonaId
	 * @param tipoZona
	 */
	public TipoHospedaje(int tipoHospedajeId, String tipoHospedaje) {
		super();
		this.tipoHospedajeId = tipoHospedajeId;
		this.tipoHospedaje = tipoHospedaje;
	}
	
	/**
	 * Constructor sin id
	 * @param tipoZona
	 */
	public TipoHospedaje(String tipoHospedaje) {
		super();
		this.tipoHospedaje = tipoHospedaje;
	}

	/**
	 * getters and setters
	 * @return
	 */
	public int getTipoHospedajeId() {
		return this.tipoHospedajeId;
	}
	
	public String getTipoHospedaje() {
		return this.tipoHospedaje;
	}
	
	public void setTipoHospedaje(String tipoHospedaje) {
		this.tipoHospedaje = tipoHospedaje;
	}
	
	public Set<Hospedaje> getRelacionHospedajes(){
		return this.relacionHospedajes;
	}
	
	@Override
	public String toString() {
		return "TipoHospedaje [tipoHospedajeId=" + tipoHospedajeId + ", tipoHospedaje=" + tipoHospedaje + "]";
	}


	/* con builder	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TipoHospedaje [tipoHospedajeId=").append(tipoHospedajeId).append(", tipoHospedaje=")
				.append(tipoHospedaje).append("]");
		return builder.toString();
	}*/
}