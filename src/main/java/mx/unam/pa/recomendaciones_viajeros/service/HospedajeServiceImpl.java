package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.dao.HospedajeDAO;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;

@Service
@Transactional
public class HospedajeServiceImpl implements HospedajeService {

	@Autowired
	private HospedajeDAO dao;

	@Override
	public void save(Hospedaje h) {
		dao.save(h);
	}

	@Override
	public void eliminateOne(Hospedaje h) {
		dao.eliminateOne(h);
	}
	
	@Override
	public void updateOne(Hospedaje h) {
		dao.updateOne(h);
	}

	@Override
	public void createWhitValues(String nombre, double latitud, double longitud, int costo) {
		Hospedaje h = new Hospedaje(nombre, latitud, longitud, costo);
		dao.save(h);
	}

	@Override
	public Hospedaje getById(int id) {
		return dao.getById(id);
	}

	@Override
	public List<Hospedaje> getAll(){
		return dao.getAll();
	}

	@Override
	public List<Hospedaje> getAllBetween(Individuo ind){
		return dao.getAllBetween(ind.calculaRectangulo());
	}

	@Override
	public List<Hospedaje> getAllBetween(double [] rectangulo){
		return dao.getAllBetween(rectangulo);
	}
}
