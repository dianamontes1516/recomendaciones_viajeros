package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;

public interface HospedajeService {

	public void save(Hospedaje h);

	public void updateOne(Hospedaje h);

	public void createWhitValues(String nombre, double latitud, double longitud, int costo);

	public List<Hospedaje> getAll();

	public Hospedaje getById(int id);

	public List<Hospedaje> getAllBetween(Individuo ind);

	public List<Hospedaje> getAllBetween(double[] rectangulo);

	public void eliminateOne(Hospedaje h);

}
