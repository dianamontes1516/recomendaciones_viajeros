package mx.unam.pa.recomendaciones_viajeros.service;

import mx.unam.pa.recomendaciones_viajeros.model.Persona;

public interface PersonaService {

	public void guardarPersona(Persona persona);
	
	public Persona getPersona(short id);

}
