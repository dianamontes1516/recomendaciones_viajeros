package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.formas.AdminZonaFrm;
import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;

public interface TipoZonaService {

	public void save(TipoZona tz);

	public void updateOne(TipoZona tz);

	public void create(String tipoZona);

	public List<TipoZona> getAll();

	void guardarTipoZona(AdminZonaFrm frmRegistro);

}
