package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.dao.TipoAtraccionDAO;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

@Service
@Transactional
public class TipoAtraccionServiceImpl implements TipoAtraccionService {

	@Autowired
	private TipoAtraccionDAO dao;

	@Override
	public void save(TipoAtraccion ta) {
		dao.save(ta);
	}

	@Override
	public void updateOne(TipoAtraccion ta) {
		dao.updateOne(ta);
	}
	
	@Override
	public void create(String tipoAtraccion) {
		TipoAtraccion ta = new TipoAtraccion(tipoAtraccion);
		dao.save(ta);
	}

	@Override
	public List<TipoAtraccion> getAll() {
		return dao.getAll();
	}

}
