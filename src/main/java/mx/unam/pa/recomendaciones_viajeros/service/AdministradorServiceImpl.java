package mx.unam.pa.recomendaciones_viajeros.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.dao.AdministradorDAO;
import mx.unam.pa.recomendaciones_viajeros.formas.AdminLoginFrm;
import mx.unam.pa.recomendaciones_viajeros.formas.RegistroValidacionPasswdFrm;
import mx.unam.pa.recomendaciones_viajeros.model.Administrador;

@Service
@Transactional
public class AdministradorServiceImpl implements AdministradorService{

	@Autowired
	private AdministradorDAO dao;
	
	@Override
	public Administrador login(AdminLoginFrm adminfrmLogin) {
		Administrador admin = new Administrador();
		admin.setUsuario(adminfrmLogin.getUsuario());
		admin.setContraseña(adminfrmLogin.getContraseña());
		
		return dao.loginAdministrador(admin);
	}

	@Override
	public Administrador guardarUsuario(RegistroValidacionPasswdFrm frmRegistro) {
		System.out.println("guardarUsuario() en service Impl Admin");
		
		// Creando un objeto Usuario a partir de la información de la forma
		Administrador nuevoAdmin = new Administrador(frmRegistro.getUsuario(), 
				frmRegistro.getContraseña());
		
		String usuarioId = dao.saveAdministrador(nuevoAdmin);
		return dao.loadAdministrador(usuarioId);
	}


}
