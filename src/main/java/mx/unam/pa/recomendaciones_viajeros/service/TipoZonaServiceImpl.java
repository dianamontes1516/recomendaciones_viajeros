package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.dao.TipoAtraccionDAO;
import mx.unam.pa.recomendaciones_viajeros.dao.TipoZonaDAO;
import mx.unam.pa.recomendaciones_viajeros.formas.AdminZonaFrm;
import mx.unam.pa.recomendaciones_viajeros.formas.RegistroValidacionPasswdFrm;
import mx.unam.pa.recomendaciones_viajeros.model.Administrador;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;

@Service
@Transactional
public class TipoZonaServiceImpl implements TipoZonaService {

	@Autowired
	private TipoZonaDAO dao;

	@Override
	public void save(TipoZona tz) {
		dao.save(tz);
	}

	@Override
	public void updateOne(TipoZona tz) {
		dao.updateOne(tz);
	}
	
	@Override
	public void create(String tipoZona) {
		TipoZona tz = new TipoZona(tipoZona);
		dao.save(tz);
	}

	@Override
	public List<TipoZona> getAll() {
		return dao.getAll();
	}


	@Override
	public void guardarTipoZona(AdminZonaFrm frmRegistro) {
		System.out.println("guardarUsuario() en service Impl " + frmRegistro.getTipoZona());
		this.create(frmRegistro.getTipoZona());
	}

}
