package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.dao.AtraccionDAO;
import mx.unam.pa.recomendaciones_viajeros.forms.AtraccionFom;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
//import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

@Service
@Transactional
public class AtraccionServiceImpl implements AtraccionService {

	@Autowired
	private AtraccionDAO dao;

	@Override
	public void save(Atraccion a) {
		dao.save(a);
	}

	@Override
	public void saveF(AtraccionFom a) {
		Atraccion atraccion = new Atraccion(a.getAtraccionId(), a.getNombre(), a.getLatitud(), a.getLongitud(),
				a.getCosto(), a.getTipoAtraccionId());
		dao.save(atraccion);
	}

	@Override
	public void eliminateOne(Atraccion a) {
		dao.eliminateOne(a);
	}

	@Override
	public void updateOne(Atraccion a) {
		dao.updateOne(a);
	}

	@Override
	public void createWithValues(String nombre, double latitud, double longitud, int costo, int tipoAtraccionId) {
		Atraccion a = new Atraccion(nombre, latitud, longitud, costo, tipoAtraccionId);
		dao.save(a);
	}

	@Override
	public Atraccion getById(int id) {
		return dao.getById(id);
	}

	@Override
	public List<Atraccion> getAll() {
		return dao.getAll();
	}

	@Override
	public List<Atraccion> getAllByType(int id) {
		return dao.getAllByType(id);
	}

	@Override
	public List<Atraccion> getAllBetweenAndType(Individuo ind, int typeId) {
		return dao.getAllBetweenAndType(ind.calculaRectangulo(), typeId);
	}

	@Override
	public List<Atraccion> getAllBetweenAndType(double[] rectangulo, int typeId) {
		return dao.getAllBetweenAndType(rectangulo, typeId);
	}

}
