package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;

public interface TipoHospedajeService {

	public void save(TipoHospedaje th);

	public void updateOne(TipoHospedaje th);

	public void create(String tipoHospedaje);

	public List<TipoHospedaje> getAll();

}
