package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.forms.AtraccionFom;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
//import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

public interface AtraccionService {

	public void save(Atraccion a);
	
	public void saveF(AtraccionFom a);

	public void updateOne(Atraccion a);

	public void createWithValues(String nombre, double longitud, double latitud, int costo, int tipoAtraccionId);

	public List<Atraccion> getAll();

	public Atraccion getById(int i);

	public List<Atraccion> getAllByType(int id);

	public List<Atraccion> getAllBetweenAndType(Individuo ind, int typeId);

	public List<Atraccion> getAllBetweenAndType(double[] rectangulo, int typeId);

	public void eliminateOne(Atraccion a);

}
