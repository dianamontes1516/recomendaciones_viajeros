package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

public interface TipoAtraccionService {

	public void save(TipoAtraccion ta);
	
	public void updateOne(TipoAtraccion ta);
	
	public void create(String tipoAtraccion);

	public List<TipoAtraccion> getAll();

}
