package mx.unam.pa.recomendaciones_viajeros.service;

import mx.unam.pa.recomendaciones_viajeros.formas.AdminLoginFrm;
import mx.unam.pa.recomendaciones_viajeros.formas.RegistroValidacionPasswdFrm;
import mx.unam.pa.recomendaciones_viajeros.model.Administrador;

public interface AdministradorService {
	
	public Administrador login(AdminLoginFrm frmLogin);
	
	/**
	 * Almacenar el administrador
	 * @param usuario
	 */
	public Administrador guardarUsuario(RegistroValidacionPasswdFrm frmRegistro);
}
