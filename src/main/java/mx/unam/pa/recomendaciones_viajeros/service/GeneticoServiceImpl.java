package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.ArrayList;
//import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.genetico.ConjuntoAtracciones;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.genetico.Maestro;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

@Service
@Transactional
public class GeneticoServiceImpl implements GeneticoService {
	
	@Autowired
	private AtraccionService AtService;


	
	@Override
	public ArrayList<ArrayList<Atraccion>> getArrayListAtracciones(ArrayList<TipoAtraccion> listaTipoAtraccion) {
		
		ArrayList<ArrayList<Atraccion>> atracciones = new ArrayList<>();
		
		for (int i = 0; i < listaTipoAtraccion.size(); i++) {
			int id_tipo = listaTipoAtraccion.get(i).getTipoAtraccionId();
			ArrayList<Atraccion> arreglo = new ArrayList<>(AtService.getAllByType(id_tipo));
			atracciones.add(arreglo);
		}
		return atracciones;
	}
	
	@Override
	public ArrayList<Individuo> getArrayMejoresAtracciones(ArrayList<TipoAtraccion> listaTipoAtraccion, int no_mejores) {
//		GeneticoService gs = new GeneticoServiceImpl();
		
		ArrayList<ArrayList<Atraccion>> atracciones = this.getArrayListAtracciones(listaTipoAtraccion);
		ConjuntoAtracciones ca = new ConjuntoAtracciones(atracciones);
		
		Maestro maestro = new Maestro(ca, 50, 100);
		return maestro.ejecutaGenetico(no_mejores);
	}
	
}
