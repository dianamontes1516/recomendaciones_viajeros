package mx.unam.pa.recomendaciones_viajeros.service;

//import java.util.List;
import java.util.ArrayList;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;

public interface GeneticoService {
	
	public ArrayList<ArrayList<Atraccion>> getArrayListAtracciones(ArrayList<TipoAtraccion> listaTipoAtraccion);

	public ArrayList<Individuo> getArrayMejoresAtracciones(ArrayList<TipoAtraccion> listaTipoAtraccion, int no_mejores);

	
}
