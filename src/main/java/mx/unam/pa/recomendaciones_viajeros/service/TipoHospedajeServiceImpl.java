package mx.unam.pa.recomendaciones_viajeros.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.unam.pa.recomendaciones_viajeros.dao.TipoAtraccionDAO;
import mx.unam.pa.recomendaciones_viajeros.dao.TipoHospedajeDAO;
import mx.unam.pa.recomendaciones_viajeros.dao.TipoZonaDAO;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;

@Service
@Transactional
public class TipoHospedajeServiceImpl implements TipoHospedajeService {

	@Autowired
	private TipoHospedajeDAO dao;

	@Override
	public void save(TipoHospedaje th) {
		dao.save(th);
	}

	@Override
	public void updateOne(TipoHospedaje th) {
		dao.updateOne(th);
	}
	
	@Override
	public void create(String tipoHospedaje) {
		TipoHospedaje th = new TipoHospedaje(tipoHospedaje);
		dao.save(th);
	}

	@Override
	public List<TipoHospedaje> getAll() {
		return dao.getAll();
	}

}
