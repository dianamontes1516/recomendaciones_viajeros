package mx.unam.pa.recomendaciones_viajeros.service;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.unam.pa.recomendaciones_viajeros.dao.PersonaDAO;
import mx.unam.pa.recomendaciones_viajeros.model.Persona;

@Service
@Transactional
public class PersonaServiceImpl implements PersonaService {

	@Autowired
	private PersonaDAO dao;

	@Override
	public void guardarPersona(Persona persona) {
		dao.save(persona);
	}

	@Override
	public Persona getPersona(short id) {
		return dao.getPersona(id);
	}

}
