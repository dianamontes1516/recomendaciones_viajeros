package mx.unam.pa.recomendaciones_viajeros.forms;

import javax.validation.constraints.Digits;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.Email;

public class AtraccionFom {

	@NotNull(message = "Nombre requerido")
	@Pattern(regexp = "^[\\pL\\pM\\p{Zs}.-]+$", message = "Nombre requerido. Sólo letras")
	private String nombre;
	
	@NotNull(message = "Latitud requerida")
	@Digits(message = "El costo debe ser un número decimal", fraction = 10, integer = 3)
	private Double latitud;
	
	@NotNull(message = "Longitud requerida")
	@Digits(message = "El costo debe ser un número decimal", fraction = 10, integer = 3)
	private Double longitud;

	@NotNull(message = "Costo requerido")
	@Digits(message = "El costo debe ser un número no mayor a 99999", fraction = 0, integer = 5)
	private Integer costo;
	
	@NotNull(message = "Tipo de atracción requerida")
	private Integer tipoAtraccionId;
	
	private Integer atraccionId;

	public AtraccionFom() {
		
	}

	public AtraccionFom(String nombre, Double latitud, Double longitud, Integer costo, Integer tipoAtraccionId,
			Integer atraccionId) {
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = costo;
		this.tipoAtraccionId = tipoAtraccionId;
		this.atraccionId = atraccionId;
	}

	public AtraccionFom(String nombre, Double latitud, Double longitud, Integer costo, Integer atraccionId) {
		this.nombre = nombre;
		this.latitud = latitud;
		this.longitud = longitud;
		this.costo = costo;
		this.atraccionId = atraccionId;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public Integer getCosto() {
		return costo;
	}

	public void setCosto(Integer costo) {
		this.costo = costo;
	}

	public Integer getTipoAtraccionId() {
		return tipoAtraccionId;
	}

	public void setTipoAtraccionId(Integer tipoAtraccionId) {
		this.tipoAtraccionId = tipoAtraccionId;
	}

	public Integer getAtraccionId() {
		return atraccionId;
	}

	public void setAtraccionId(Integer atraccionId) {
		this.atraccionId = atraccionId;
	}
	
	
	
}
