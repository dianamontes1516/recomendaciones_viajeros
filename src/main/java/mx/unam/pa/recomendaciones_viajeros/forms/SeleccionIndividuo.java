package mx.unam.pa.recomendaciones_viajeros.forms;

import java.util.ArrayList;
import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;

public class SeleccionIndividuo {

	public ArrayList<Individuo> individuos;
	
	public List<Integer> seleccion;

	public SeleccionIndividuo() {
		
	}
	
	public SeleccionIndividuo(ArrayList<Individuo> li) {
		this.individuos = li;
	}
	
	
	@Override
	public String toString() {
		return "SeleccionIndividuo [individuos=" + individuos + ", seleccion=" + seleccion + "]";
	}

	public ArrayList<Individuo> getIndividuos() {
		return individuos;
	}

	public void setIndividuos(ArrayList<Individuo> individuos) {
		this.individuos = individuos;
	}

	public List<Integer> getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(List<Integer> seleccion) {
		this.seleccion = seleccion;
	}

	
	
	
	
}
