package mx.unam.pa.recomendaciones_viajeros.forms;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

public class SeleccionTipoAtraccion {
	
	public List<TipoAtraccion> catalogo;
	
	public List<String> seleccionTipos;

	public List<TipoAtraccion> getCatalogo() {
		return catalogo;
	}

	public void setCatalogo(List<TipoAtraccion> catalogo) {
		this.catalogo = catalogo;
	}

	public List<String> getSeleccionTipos() {
		return seleccionTipos;
	}

	public void setSeleccionTipos(List<String> seleccionTipos) {
		this.seleccionTipos = seleccionTipos;
	}
	
	@Override
	public String toString() {
		return "SeleccionTipoAtraccion [catalogo=" + catalogo + ", seleccionTipos=" + seleccionTipos + "]";
	}

}
