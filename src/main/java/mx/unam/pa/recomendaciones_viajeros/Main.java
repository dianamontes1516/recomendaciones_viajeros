package mx.unam.pa.recomendaciones_viajeros;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import mx.unam.pa.recomendaciones_viajeros.config.HibernateConfig;
import mx.unam.pa.recomendaciones_viajeros.genetico.Individuo;
import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;
import mx.unam.pa.recomendaciones_viajeros.service.AtraccionService;
import mx.unam.pa.recomendaciones_viajeros.service.GeneticoService;
import mx.unam.pa.recomendaciones_viajeros.service.HospedajeService;
import mx.unam.pa.recomendaciones_viajeros.service.TipoHospedajeService;
import mx.unam.pa.recomendaciones_viajeros.service.TipoZonaService;


public class Main {

//	public void createPrueba() {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			servicio.createWithValues("nombrePrueba", 1,1,0,1);
//
//		}
//	}
//	
//	public void updatePrueba(Atraccion atraccion) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			servicio.update(atraccion);
//
//		}
//	}
//	
//	public boolean atExistIdPrueba(int id) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			return servicio.existId(id);
//		}
//	}
//
//	public Atraccion getByIdPrueba(int id) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			return servicio.getById(id);
//		}
//	}
//
//	public List<Atraccion> getAllPrueba() {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			return servicio.getAll();
//		}
//	}
//	
//	public List<Atraccion> getAllByZonePrueba(int id) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			return servicio.getAllByZone(id);
//		}
//	}
//
//	public List<Atraccion> getAllByTypePrueba(int id) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			AtraccionService servicio = context.getBean(AtraccionService.class);
//			return servicio.getAllByType(id);
//		}
//	}
//
//	public ArrayList<ArrayList<Atraccion>> getArrayListAtraccionesPrueba(ArrayList<TipoAtraccion> listaTipoAtraccion) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			GeneticoService servicio = context.getBean(GeneticoService.class);
//			return servicio.getArrayListAtracciones(listaTipoAtraccion);
//		}
//	}
//	
//	public ArrayList<Individuo> getArrayMejoresAtraccionesPrueba(ArrayList<TipoAtraccion> listaTipoAtraccion, int no_mejores) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			GeneticoService servicio = context.getBean(GeneticoService.class);
//			return servicio.getArrayMejoresAtracciones(listaTipoAtraccion,no_mejores);
//		}
//	}
//
//	public List<TipoZona> getAllZonaPrueba() {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			TipoZonaService servicio = context.getBean(TipoZonaService.class);
//			return servicio.getAll();
//		}
//	}
//
//	public List<TipoHospedaje> getAllTipoHospedajePrueba() {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			TipoHospedajeService servicio = context.getBean(TipoHospedajeService.class);
//			return servicio.getAll();
//		}
//	}
//
//	public Hospedaje getByIdHospedajePrueba(int id) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			HospedajeService servicio = context.getBean(HospedajeService.class);
//			return servicio.getById(id);
//		}
//	}
//
//	public List<Hospedaje> getAllBetweenPrueba(Individuo ind) {
//		try(AnnotationConfigApplicationContext context = 
//				new AnnotationConfigApplicationContext(HibernateConfig.class) ){
//
//			HospedajeService servicio = context.getBean(HospedajeService.class);
//			return servicio.getAllBetween(ind);
//		}
//	}
//
//	public static void main(String[] args) {
//		Main m = new Main();
//		
//		Atraccion atraccion = new Atraccion(1, 10.00 , 17.00);
//		
//		m.updatePrueba(atraccion);
//		
//		
///*		
//		System.out.println(m.atExistIdPrueba(100));
//
//		System.out.println(m.getByIdPrueba(100).toString());
//
////prueba between
// * 
//		double[] rectangulo = {20.0, 21.0, -90.0, -89.0};
//		System.out.println(m.getAllBetweenPrueba(rectangulo));
//		
//		
//		
//		//prueba de genetico
//				ArrayList<TipoAtraccion> listaTipoAtraccion = new ArrayList<>();
//				TipoAtraccion tipo1 = new TipoAtraccion(1,"tipo1");
//				TipoAtraccion tipo2 = new TipoAtraccion(5,"tipo2");
//				listaTipoAtraccion.add(tipo1);
//				listaTipoAtraccion.add(tipo2);
//				System.out.println(m.getArrayMejoresAtraccionesPrueba(listaTipoAtraccion,6).toString());
//
//		
//		m.createPrueba();
//		
//		System.out.println(m.getAllPrueba().toString());
//		System.out.println(m.getByIdPrueba(2).toString());
//
//		System.out.println(m.getAllByTypePrueba(13).toString());
//
//
////Probar consulta tipo hospedaje lista		
//		System.out.println(m.getAllTipoHospedajePrueba().toString());
//		System.out.println(m.getByIdHospedajePrueba(2).toString());
//		System.out.println(m.getByIdHospedajePrueba(2).getNombre().toString());
//		System.out.println(m.getByIdHospedajePrueba(2).getRelacionTipoHospedaje().toString());
//
////		System.out.println(m.getAllByZonePrueba(1).toString());
//
//*/	
//	}

}
