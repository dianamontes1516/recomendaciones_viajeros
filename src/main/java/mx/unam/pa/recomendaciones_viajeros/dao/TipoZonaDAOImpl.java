package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;
import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;

@Repository
public class TipoZonaDAOImpl implements TipoZonaDAO {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public void save(TipoZona tz) {
		sessionFactory.getCurrentSession().save(tz);
	}

	@Override
	public void updateOne(TipoZona tz) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaUpdate<TipoZona> criteriaUpdate = builder.createCriteriaUpdate(TipoZona.class);
		Root<TipoZona> root = criteriaUpdate.from(TipoZona.class);
		criteriaUpdate.set("tipoZona", tz.getTipoZona());
		criteriaUpdate.where(builder.equal(root.get("tipoZonaId"), tz.getTipoZonaId()));
		session.createQuery(criteriaUpdate).executeUpdate();
	}

	//Ordenadas por el nombre
	@Override
	public List<TipoZona> getAll(){
		Session session = sessionFactory.getCurrentSession();		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<TipoZona> criteria = builder.createQuery(TipoZona.class);
		Root<TipoZona> rootTipoZona = criteria.from(TipoZona.class);
		criteria.select(rootTipoZona);
		criteria.orderBy(builder.asc(rootTipoZona.get("tipoZona")));
		TypedQuery<TipoZona> query = session.createQuery(criteria);
		List<TipoZona> result = query.getResultList();		
		return result;
	}

}
