package mx.unam.pa.recomendaciones_viajeros.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Persona;

@Repository
public class PersonaDAOImpl implements PersonaDAO {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public void save(Persona persona) {
		sessionFactory.getCurrentSession().save(persona);
	}

	@Override
	public void updateOne(Persona p) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaUpdate<Persona> criteriaUpdate = builder.createCriteriaUpdate(Persona.class);
		Root<Persona> root = criteriaUpdate.from(Persona.class);
		criteriaUpdate.set("nombre", p.getNombre());
		criteriaUpdate.set("apPaterno", p.getApPaterno());
		criteriaUpdate.set("apMaterno", p.getApMaterno());
		criteriaUpdate.set("correo", p.getCorreo());
		criteriaUpdate.where(builder.equal(root.get("personaId"), p.getPersonaId()));
		session.createQuery(criteriaUpdate).executeUpdate();
	}

	@Override
	public Persona getPersona(short id) {
		return sessionFactory.getCurrentSession().get(Persona.class, id);
	}

}
