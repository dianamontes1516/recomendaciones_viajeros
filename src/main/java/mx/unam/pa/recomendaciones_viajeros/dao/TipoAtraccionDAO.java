package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

public interface TipoAtraccionDAO {
	
	public void save(TipoAtraccion ta);
	
	public void updateOne(TipoAtraccion ta);

	public List<TipoAtraccion> getAll();

}
