package mx.unam.pa.recomendaciones_viajeros.dao;

import mx.unam.pa.recomendaciones_viajeros.model.Persona;

public interface PersonaDAO {
	
	/**
	 * <p>Ejecución de inserción en la tabla persona.</p>
	 * @param persona
	 */
	public void save(Persona persona);
	
	public void updateOne(Persona p);

	/**
	 * <p>Ejecución de consulta a la tabla persona con el criterio id.</p>
	 * @param id
	 * @return Perosona
	 */
	public Persona getPersona(short id );

}
