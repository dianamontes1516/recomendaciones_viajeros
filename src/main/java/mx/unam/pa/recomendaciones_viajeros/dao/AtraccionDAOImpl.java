package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;

@Repository
public class AtraccionDAOImpl implements AtraccionDAO {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public void save(Atraccion a) {
		sessionFactory.getCurrentSession().save(a);
	}

	@Override
	public void eliminateOne(Atraccion a) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaDelete<Atraccion> criteriaDel = builder.createCriteriaDelete(Atraccion.class);
		Root<Atraccion> rootAt = criteriaDel.from(Atraccion.class);
		criteriaDel.where(builder.equal(rootAt.get("atraccionId"), a.getAtraccionId()));
		session.createQuery(criteriaDel).executeUpdate();
	}
	
	
	@Override
	public void updateOne(Atraccion a) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaUpdate<Atraccion> criteriaUpdate = builder.createCriteriaUpdate(Atraccion.class);
		Root<Atraccion> rootAt = criteriaUpdate.from(Atraccion.class);
		criteriaUpdate.set("nombre", a.getNombre());
		criteriaUpdate.set("latitud", a.getLatitud());
		criteriaUpdate.set("longitud", a.getLongitud());
		criteriaUpdate.set("costo", a.getCosto());
		criteriaUpdate.set("tipoAtraccionId", a.getTipoAtraccionId());
		criteriaUpdate.where(builder.equal(rootAt.get("atraccionId"), a.getAtraccionId()));
		session.createQuery(criteriaUpdate).executeUpdate();
	}

	@Override
	public Atraccion getById(int id) {
		return sessionFactory.getCurrentSession().get(Atraccion.class, id);
	}

	@Override
	public List<Atraccion> getAll(){
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Atraccion> criteria = builder.createQuery(Atraccion.class);
		Root<Atraccion> rootAtraccion = criteria.from(Atraccion.class);
		criteria.select(rootAtraccion);
		criteria.orderBy(builder.asc(rootAtraccion.get("nombre")));
		TypedQuery<Atraccion> query = session.createQuery(criteria);
		List<Atraccion> result = query.getResultList();
		return result;
	}

	@Override
	public List<Atraccion> getAllByType(int id) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Atraccion> criteria = builder.createQuery(Atraccion.class);
		Root<Atraccion> root = criteria.from(Atraccion.class);
		criteria.select(root).where(
				builder.equal(root.get("tipoAtraccionId"), id)
			);
		Query<Atraccion> query = session.createQuery(criteria);
		List<Atraccion> tas = query.getResultList();

		return tas;
	}

	@Override
	public List<Atraccion> getAllBetweenAndType(double [] rectangulo, int typeId){
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Atraccion> criteria = builder.createQuery(Atraccion.class);
		Root<Atraccion> rootAtraccion = criteria.from(Atraccion.class);
		criteria.where(
				builder.and(
						builder.between(rootAtraccion.get("latitud"),rectangulo[0],rectangulo[1])
						,builder.between(rootAtraccion.get("longitud"), rectangulo[2], rectangulo[3])
						,builder.equal(rootAtraccion.get("tipoAtraccionId"), typeId)
						));
		criteria.orderBy(builder.asc(rootAtraccion.get("nombre")));
		TypedQuery<Atraccion> query = session.createQuery(criteria);
		List<Atraccion> result = query.getResultList();
		return result;
	}

}
