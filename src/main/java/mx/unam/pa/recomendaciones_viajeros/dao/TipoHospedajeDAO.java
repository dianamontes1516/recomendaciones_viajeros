package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;

public interface TipoHospedajeDAO {

	public void save(TipoHospedaje th);

	public void updateOne(TipoHospedaje th);

	public List<TipoHospedaje> getAll();

}
