package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;

public interface HospedajeDAO {

	public void save(Hospedaje a);

	public Hospedaje getById(int id);

	public List<Hospedaje> getAll();

	public List<Hospedaje> getAllBetween(double[] rectangulo);

	public void updateOne(Hospedaje h);

	public void eliminateOne(Hospedaje h);

}
