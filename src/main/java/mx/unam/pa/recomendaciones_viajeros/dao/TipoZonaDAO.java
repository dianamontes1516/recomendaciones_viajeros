package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.TipoZona;

public interface TipoZonaDAO {

	public void save(TipoZona tz);

	public void updateOne(TipoZona tz);

	public List<TipoZona> getAll();

}
