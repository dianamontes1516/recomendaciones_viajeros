package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

public interface AtraccionDAO {

	//Crear nueva.
	public void save(Atraccion a);

	//Obtener por id
	public Atraccion getById(int id);

	public List<Atraccion> getAll();

	//obtener todos las atracciones con un tipo_atraccion_id
	public List<Atraccion> getAllByType(int id);

	public List<Atraccion> getAllBetweenAndType(double[] rectangulo, int typeId);

	public void updateOne(Atraccion a);

	public void eliminateOne(Atraccion a);



}
