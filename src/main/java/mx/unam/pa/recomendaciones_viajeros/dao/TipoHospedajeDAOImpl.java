package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoHospedaje;

@Repository
public class TipoHospedajeDAOImpl implements TipoHospedajeDAO {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public void save(TipoHospedaje th) {
		sessionFactory.getCurrentSession().save(th);
	}

	@Override
	public void updateOne(TipoHospedaje th) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaUpdate<TipoHospedaje> criteriaUpdate = builder.createCriteriaUpdate(TipoHospedaje.class);
		Root<TipoHospedaje> root = criteriaUpdate.from(TipoHospedaje.class);
		criteriaUpdate.set("tipoHospedaje", th.getTipoHospedaje());
		criteriaUpdate.where(builder.equal(root.get("tipoHospedajeId"), th.getTipoHospedajeId()));
		session.createQuery(criteriaUpdate).executeUpdate();
	}

	//Ordenadas por el nombre
	@Override
	public List<TipoHospedaje> getAll(){
		Session session = sessionFactory.getCurrentSession();		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<TipoHospedaje> criteria = builder.createQuery(TipoHospedaje.class);
		Root<TipoHospedaje> rootTipoHospedaje = criteria.from(TipoHospedaje.class);
		criteria.select(rootTipoHospedaje);
		criteria.orderBy(builder.asc(rootTipoHospedaje.get("tipoHospedaje")));
		TypedQuery<TipoHospedaje> query = session.createQuery(criteria);
		List<TipoHospedaje> result = query.getResultList();		
		return result;
	}

}
