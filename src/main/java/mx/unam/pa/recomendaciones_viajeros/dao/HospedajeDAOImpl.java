package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;

@Repository
public class HospedajeDAOImpl implements HospedajeDAO {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public void save(Hospedaje a) {
		sessionFactory.getCurrentSession().save(a);
	}

	@Override
	public void eliminateOne(Hospedaje h) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaDelete<Hospedaje> criteriaDel = builder.createCriteriaDelete(Hospedaje.class);
		Root<Hospedaje> root = criteriaDel.from(Hospedaje.class);
		criteriaDel.where(builder.equal(root.get("hospedajeId"), h.getHospedajeId()));
		session.createQuery(criteriaDel).executeUpdate();
	}
	
	@Override
	public void updateOne(Hospedaje h) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaUpdate<Hospedaje> criteriaUpdate = builder.createCriteriaUpdate(Hospedaje.class);
		Root<Hospedaje> root = criteriaUpdate.from(Hospedaje.class);
		criteriaUpdate.set("nombre", h.getNombre());
		criteriaUpdate.set("latitud", h.getLatitud());
		criteriaUpdate.set("longitud", h.getLongitud());
		criteriaUpdate.set("costo", h.getCosto());
		criteriaUpdate.where(builder.equal(root.get("hospedajeId"), h.getHospedajeId()));
		session.createQuery(criteriaUpdate).executeUpdate();
	}

	@Override
	public Hospedaje getById(int id) {
		return sessionFactory.getCurrentSession().get(Hospedaje.class, id);
	}
	
	@Override
	public List<Hospedaje> getAll(){
		Session session = sessionFactory.getCurrentSession();		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Hospedaje> criteria = builder.createQuery(Hospedaje.class);
		Root<Hospedaje> rootHospedaje = criteria.from(Hospedaje.class);
		criteria.select(rootHospedaje);
		criteria.orderBy(builder.asc(rootHospedaje.get("nombre")));
		TypedQuery<Hospedaje> query = session.createQuery(criteria);
		List<Hospedaje> result = query.getResultList();		
		return result;
	}

	@Override
	public List<Hospedaje> getAllBetween(double [] rectangulo){
		Session session = sessionFactory.getCurrentSession();		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Hospedaje> criteria = builder.createQuery(Hospedaje.class);
		Root<Hospedaje> rootHospedaje = criteria.from(Hospedaje.class);
		criteria.where(
				builder.and(
						builder.between(rootHospedaje.get("latitud"),rectangulo[0],rectangulo[1])
						,builder.between(rootHospedaje.get("longitud"), rectangulo[2], rectangulo[3])
						));
		criteria.orderBy(builder.asc(rootHospedaje.get("nombre")));
		TypedQuery<Hospedaje> query = session.createQuery(criteria);
		List<Hospedaje> result = query.getResultList();		
		return result;
	}

}

