package mx.unam.pa.recomendaciones_viajeros.dao;

import java.util.List;

import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.CriteriaUpdate;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.Atraccion;
import mx.unam.pa.recomendaciones_viajeros.model.TipoAtraccion;

@Repository
public class TipoAtraccionDAOImpl implements TipoAtraccionDAO {

	@Autowired
    private SessionFactory sessionFactory;

	@Override
	public void save(TipoAtraccion ta) {
		sessionFactory.getCurrentSession().save(ta);
	}

	@Override
	public void updateOne(TipoAtraccion ta) {
		Session session = sessionFactory.getCurrentSession();
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaUpdate<TipoAtraccion> criteriaUpdate = builder.createCriteriaUpdate(TipoAtraccion.class);
		Root<TipoAtraccion> root = criteriaUpdate.from(TipoAtraccion.class);
		criteriaUpdate.set("tipoAtraccion", ta.getTipoAtraccion());
		criteriaUpdate.where(builder.equal(root.get("tipoAtraccionId"), ta.getTipoAtraccionId()));
		session.createQuery(criteriaUpdate).executeUpdate();
	}

	@Override
	public List<TipoAtraccion> getAll(){
		Session session = sessionFactory.getCurrentSession();		
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<TipoAtraccion> criteria = builder.createQuery(TipoAtraccion.class);
		Root<TipoAtraccion> rootTipoAtraccion = criteria.from(TipoAtraccion.class);
		criteria.select(rootTipoAtraccion);
		criteria.orderBy(builder.asc(rootTipoAtraccion.get("tipoAtraccion")));
		TypedQuery<TipoAtraccion> query = session.createQuery(criteria);
		List<TipoAtraccion> result = query.getResultList();		
		return result;
	}

}
