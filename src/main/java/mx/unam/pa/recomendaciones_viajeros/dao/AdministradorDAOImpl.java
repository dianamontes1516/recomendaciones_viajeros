package mx.unam.pa.recomendaciones_viajeros.dao;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import mx.unam.pa.recomendaciones_viajeros.model.Administrador;

@Repository
public class AdministradorDAOImpl implements AdministradorDAO{

	@Autowired
    private SessionFactory sessionFactory;
	
	@Override
	public String saveAdministrador(Administrador admin) {
		System.out.println("saveAdministrador()");
		Session session = sessionFactory.getCurrentSession();
		return (String) session.save(admin);		
	}

	@Override
	public Administrador getById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Administrador loginAdministrador(Administrador admin) {
		Session session = sessionFactory.getCurrentSession(); 
		CriteriaBuilder builder = session.getCriteriaBuilder();
		CriteriaQuery<Administrador> criteria = builder.createQuery(Administrador.class);
		
		Root<Administrador> root = criteria.from(Administrador.class);
		
		criteria.select(root).where(
				builder.and( // definición de la condición AND para la cláusula de autenticacion
						builder.equal(root.get("usuario"), admin.getUsuario()),
						builder.equal(root.get("contraseña"), admin.getContraseña())
				)
			);
		// Ejecución del query
		Query<Administrador> query = session.createQuery(criteria);
		return query.uniqueResult();
	}

	@Override
	public Administrador loadAdministrador(String nombre) {
		System.out.println("loadAdministrador()");
		
		System.out.println("Recuperando el usuario por su PK: " + nombre);
		Session session = sessionFactory.getCurrentSession();
		return session.load(Administrador.class, nombre);
	}
}
