package mx.unam.pa.recomendaciones_viajeros.dao;

import mx.unam.pa.recomendaciones_viajeros.model.Administrador;

public interface AdministradorDAO {

	/**
	 * Insertar el registro del nuevo adiministrador en la tabla correspondiente
	 * @param administrador
	 */
	public String saveAdministrador(Administrador admin);
	
	public Administrador getById(int id);

	/**
	 * Búsqueda de un administrador empleando su nombre y contraseña.
	 * @param Administrador
	 * @return Objeto admin completo
	 */
	public Administrador loginAdministrador(Administrador admin);
		
	/**
	 * Recuperación del usuario por su llave primaria
	 * @param email
	 * @return
	 */
	public Administrador loadAdministrador(String nombre);
}