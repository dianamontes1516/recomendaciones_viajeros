package mx.unam.pa.recomendaciones_viajeros.formas;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class AdminZonaFrm {

	@NotNull
	@Size(min = 2, max = 20, message = "Ingresa nombre de zona")
	private String tipoZona;

	public String getTipoZona() {
		return this.tipoZona;
	}

	public void setTipoZona(String tipoZona) {
		this.tipoZona = tipoZona;
	}
	
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("AdminZonaFrm [tipoZona=").append(this.tipoZona).append("]");
		return builder.toString();
	}
}