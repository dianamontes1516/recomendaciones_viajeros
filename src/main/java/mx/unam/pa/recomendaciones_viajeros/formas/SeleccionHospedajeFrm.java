package mx.unam.pa.recomendaciones_viajeros.formas;

import java.util.ArrayList;
import java.util.List;

import mx.unam.pa.recomendaciones_viajeros.model.Hospedaje;

public class SeleccionHospedajeFrm {

	public List<Hospedaje> hospedajes;
	
	public int seleccion;

	public SeleccionHospedajeFrm() {
		
	}
	
	public SeleccionHospedajeFrm(ArrayList<Hospedaje> lh) {
		this.hospedajes = lh;
	}
	
	
	@Override
	public String toString() {
		return "SeleccionHospedajeFrm [hospedajes=" + hospedajes + ", seleccion=" + seleccion + "]";
	}

	public List<Hospedaje> getHospedajes() {
		return hospedajes;
	}

	public void setHospedajes(List<Hospedaje> hospedajes) {
		this.hospedajes = hospedajes;
	}

	public int getSeleccion() {
		return seleccion;
	}

	public void setSeleccion(int seleccion) {
		this.seleccion = seleccion;
	}

	
}
