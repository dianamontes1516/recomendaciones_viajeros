package mx.unam.pa.recomendaciones_viajeros.formas;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


public class AdminLoginFrm {

		@NotNull
		@Size(min = 2, max = 20, message = "Ingresa nombre de usuario entre 2 y 20 caracteres")
		private String usuario;
		
		@NotNull
		@Size(min = 2, max = 20, message = "Ingresa contraseña correcta")
		private String contraseña;
		
		public String getUsuario() {
			return usuario;
		}
		public void setUsuario(String usuario) {
			this.usuario = usuario;
		}
		
		public String getContraseña() {
			return contraseña;
		}
		
		public void setContraseña(String contrasenia) {
			this.contraseña = contrasenia;
		}
		
		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("LoginFrm [usuario=").append(this.usuario).append(", contraseña=").append(this.contraseña).append("]");
			return builder.toString();
		}
}
