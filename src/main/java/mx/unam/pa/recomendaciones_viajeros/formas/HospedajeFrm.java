package mx.unam.pa.recomendaciones_viajeros.formas;

import javax.validation.constraints.DecimalMax;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class HospedajeFrm {
	
	@NotNull
	@Size(min = 2, max = 80, message = "Ingresa nombre del hospedaje")
	private String nombre;
	
	@NotNull
	@DecimalMin(value = "-90.0", inclusive = true, message = "Latitud entre -90 y 90")
	@DecimalMax(value = "90.0", inclusive = true, message= "Latitud entre -90 y 90")	
	private double latitud;
	
	@NotNull
	@DecimalMin(value = "-180.0", inclusive = true, message = "Longitud entre -180 y 180")
	@DecimalMax(value = "180.0", inclusive = true, message = "Longitud entre -180 y 180")
	private double longitud;
	
	
	@NotNull
	@DecimalMin(value = "0.0", inclusive = true, message = "Solo números positivos en costo")
	@DecimalMax(value = "100000.0", inclusive = true, message = "Menores a 100k")
	private double costo;
	
	private int seleccion;
	
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setLatitud(double latitud) {
		this.latitud = latitud;
	}
	
	public void setLongitud(double longitud) {
		this.longitud = longitud;
	}
	
	public void setCosto(double costo) {
		this.costo = costo;
	}
	
	public void setSeleccion(int s) {
		this.seleccion = s;
	}
	
	public int getSeleccion() {
		return this.seleccion;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public double getLatitud() {
		return this.latitud;
	}
	
	public double getLongitud() {
		return this.longitud;
	}
	
	public double getCosto() {
		return this.costo;
	}
	
	@Override
	public String toString() {
		return "AdminHospedajeFrm [nombre=" + this.nombre + ", latitud=" + this.latitud + 
				", longitud=" + this.longitud + ", costo=" + this.costo + "]";
	}
	
}
