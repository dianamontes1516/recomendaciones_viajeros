package mx.unam.pa.recomendaciones_viajeros.formas;

import javax.validation.constraints.Pattern;

public class RegistroValidacionPasswdFrm{
	
	/**
	 * Expresión regular para validar que la cadena sean letras de la A a la Z incluyendo ñ y acentos
	 */
	@Pattern(regexp = "^[\\pL\\pM\\p{Zs}.-]+$", message = "Nombre requerido. Solo letras")
	private String usuario;
	
	private String contraseña;
	
	public RegistroValidacionPasswdFrm() {}

	public RegistroValidacionPasswdFrm(String usuario, String nuevoPassword) {
		this.usuario = usuario;
		this.contraseña = nuevoPassword;
	}

	public String getContraseña() {
		return this.contraseña;
	}

	public void setContraseña(String passwd) {
		this.contraseña = passwd;
	}
	
	public String getUsuario() {
		return this.usuario;
	}

	public void setUsuario(String usr) {
		this.usuario = usr;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("RegistroValidacionPasswdFrm [contraseña=").append(getContraseña()).append(", usuario=")
				.append(getUsuario()).append("]");
		return builder.toString();
	}
}
