# Proyecto Entrega Final
Proyecto de programación avanzada, PCIC UNAM.

El objetivo es generar recomendaciones de lugares para visitar según un conjunto de preferencias introducido por los usuarios.

## Integrantes
* Daniel Embarcadero 
* Lilian Castillo
* Diana Montes

## Setup DB
 
Usamos *mysql* como manejador de la base de datos. Ajustar _src/main/resources/hibernate.cfg_

```
		<property name="connection.username">pa</property>
		<property name="connection.password">pwd</property>

```
Con los valores adecuados.

El esquema y las tuplas de prueba se encuentran en :

```
|-> resources
  |-> ValoresDB.sql
```
## Diagrama DB  
```
|-> resources
  |-> modelo.sql
```

## Presentación
```
|-> resources
  |-> RecomendacionesViajerosEntrega.pdf
```
