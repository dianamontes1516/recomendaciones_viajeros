drop database viajes;
create database viajes;
use viajes;

create table persona(
  persona_id serial not null primary key,
  nombre varchar(200) not null,
  apellido_paterno varchar(200) not null,
  apellido_materno varchar(200),
  correo varchar(200) not null,

  unique(correo)
);

create table administrador(
   persona_id bigint unsigned not null primary key,
   usuario varchar(200) not null,
   contraseña varchar(200) not null,

   foreign key (persona_id) references persona(persona_id),
   unique(usuario)
 );

-- atraccion, tipos y zonas
create table tipo_atraccion(
  tipo_atraccion_id serial not null primary key,
  tipo_atraccion varchar(200) not null
);

create table atraccion(
  atraccion_id serial not null PRIMARY KEY,
  nombre varchar(200) not null,
  latitud double not null,
  longitud double not null,
  costo integer,
  tipo_atraccion_id bigint unsigned not null,

  foreign key (tipo_atraccion_id) references tipo_atraccion(tipo_atraccion_id)
);

create table tipo_zona(
  tipo_zona_id serial not null primary key,
  tipo_zona varchar(200) not null
);

create table atraccion_zona(
  atraccion_id bigint unsigned not null,
  tipo_zona_id bigint unsigned not null,

  foreign key (atraccion_id) references atraccion(atraccion_id) on delete cascade,
  foreign key (tipo_zona_id) references tipo_zona(tipo_zona_id) on delete cascade,
  unique(atraccion_id, tipo_zona_id)
);

-- hospedaje y tipo hospedaje
create table hospedaje(
  hospedaje_id serial not null primary key,
  nombre varchar(200) not null,
  latitud double not null,
  longitud double not null,
  costo integer
);

create table tipo_hospedaje(
  tipo_hospedaje_id serial not null primary key,
  tipo_hospedaje varchar(200) not null
);

create table hospedaje_tipos (
  hospedaje_id bigint unsigned not null,
  tipo_hospedaje_id bigint unsigned not null,

  foreign key (hospedaje_id) references hospedaje(hospedaje_id),
  foreign key (tipo_hospedaje_id) references tipo_hospedaje(tipo_hospedaje_id),
  unique (hospedaje_id, tipo_hospedaje_id)
);

-- -- -- -- --  agregar valores
-- Personas
insert into persona(nombre,apellido_paterno,apellido_materno,correo) values
  ('juan','perez','gómez','foo@vaz.com'),
  ('Lilian','Castillo','Gutierrez','foo@foo.com'),
  ('Daniel','Embarcadero','Ruiz','foo@baz.com'),
  ('Diana','Montes','Aguilar','bla@bla.com');

-- Administrador
insert into administrador(persona_id, usuario, contraseña) values
  (2, 'lilian', 'pwd_sin_cifrar'),
  (3, 'daniel', 'pwd_sin_cifrar'),
  (4, 'diana', 'pwd_sin_cifrar');

-- Catálogos
insert into tipo_atraccion (tipo_atraccion) values
  ('Festival'), -- 1
  ('Temazcal/Spa'), -- 2
  ('Shopping'), -- 3
  ('Rapel'), -- 4
  ('Globos aerostáticos'), -- 5
  ('Salto en paracaídas'), -- 6
  ('Parapente'), -- 7
  ('Parques acuáticos'), -- 8
  ('Ríos'), -- 9
  ('Cenotes'), -- 10
  ('Caminatas'), -- 11
  ('Ruinas aqueológicas'), -- 12
  ('Museos'), -- 13
  ('Minas'), -- 14
  ('Monumentos históricos'), -- 15
  ('Reserva ecologica'), -- 16
  ('Grutas'), -- 17
  ('Mirador'); -- 18

insert into tipo_zona (tipo_zona) values
  ('Playa'),
  ('Montaña'),
  ('Desierto'),
  ('Selva'),
  ('Pueblo Mágico'),
  ('Ciudad'),
  ('Lago'),
  ('Pueblo'),
  ('Bosque');

insert into tipo_hospedaje (tipo_hospedaje) values
  ('Campamento'),
  ('Hostal'),
  ('Todo incluido'),
  ('Hotel');

-- Atracciones
insert into atraccion (nombre, latitud, longitud, costo, tipo_atraccion_id) values
-- Primer conjunto, datos Diana 1-10
  ('Catedral de San Ildelfoonso', 20.9672209,-89.6250672, 0, 15), -- monumento histórico
  ('Dzibilchaltún', 21.0986014,-89.6050127, 75, 12), --
  ('Xlacah', 21.0986014,-89.6050127, 75, 10),
  ('Balneario Yucalpeten',21.2782832,-89.707831, 50, 8),
  ('Celestún',20.8579676,-90.3835375, 300,16),
  ('Grutas de Lol-tún',20.2535894,-89.458076,60,17),
  ('Ek-Balam',20.8922935,-88.1381227, 75, 12), --
  ('Calakmul Reserva Ecológica',18.1140286,-89.8248982,75,16),
  ('Calakmul Ruinas',18.1140286,-89.8248982,75,12),
  ('Bacalar',18.8065929,-89.5113794,0,9),
-- Segundo conjunto
-- 1  ('Festival'), 11- 13
  ('Feria del pan de queso', 19.9350819,-97.9633421, 0, 1),
  ('Feria de la piñata', 19.639224,-98.9161984, 0, 1),
  ('Feria de la pirotecnia', 19.687171,-99.1142888, 0,1),
-- 2  ('Temazcal/Spa'), 14-21
  ('Temazcal El Sahadai', 18.980552,-99.1264845, 400, 2),
  ('Temzacal Tlatzonki', 18.9922392,-99.2586637, 350, 2),
  ('Temazcal tradicional', 16.9200432,-96.6823732, 500, 2),
  ('Temazcal Hotel la estancia', 21.8156326,-100.0408907, 650, 2),
  ('Playa Bacocho', 15.8673611, -97.099339, 500, 2), -- spa
  ('Laguna Manialtepec', 15.9393611,-97.2121151, 100, 2), -- temazcal
  ('Parque Nacional Lagunas de Chacahua', 15.9809872,-97.7227507, 100, 2), -- temazcal, Playa 1
  ('Parque Nacional Gogorrón', 21.9860884,-101.0033143, 100, 2), -- temazcal, selva 4
-- 3  ('Shopping'),
  ('Plaza universidad', 19.3670443,-99.168202, 0, 3),
  ('Galerias Perinorte', 19.5790295,-99.2331411, 0, 3),
  ('Perisur', 19.3041091,-99.1922812, 0 ,3),
  ('Plaza La Joya', 18.9922129,-99.2586639, 0,3),
  ('Plaza Oaxaca', 16.9196616,-96.6823767, 0,3),
  ('Playa Bacocho', 15.8673611, -97.099339, 0, 3), -- shopping
-- 4  ('Rapel'),
  ('Rapel en Sotano de las Golondrinas', 21.5998365,-99.1011536, 3500, 4),
  ('Rapel en Zimatan', 15.9397217,-96.0340325, 2000, 4),
  ('Rapel en Minas Viejas', 22.3755213,-99.3202314, 1500, 4),
  ('The Muro', 19.3636447,-99.1529281, 300, 4),
  ('Parque Nacional Gogorrón', 21.9860884,-101.0033143, 100, 4), -- rapel
-- 5  ('Globos aerostáticos'),
  ('Globos aerostaticos Teotihuacan', 19.6955635,-98.8932701, 2500, 5),
  ('Globos aerostaticos Leon', 21.1880797,-101.6863166, 3000, 5),
  ('Globos aerostaticos Tequesquitengo', 18.6193675,-99.3035459, 2500, 5),
-- 6  ('Salto en paracaídas'),
  ('Skydive Tequesquitengo', 18.647693,-99.2668577, 2000, 6),
  ('Paracaidismo Celaya', 20.5453721,-100.8898404, 1800, 6),
  ('Skydive Puerto Escondido', 15.8512493,-97.0561037, 2000, 6),
-- 7  ('Parapente'),
  ('Alas del hombre', 23.3648752,-111.5768862, 2500, 7),
  ('Parapente El Salto',20.9332111,-100.2529149, 3000, 7),
  ('Tapalpa Vuela Parapente', 19.9421268,-103.6567415, 2000, 7),
  ('Playa Bacocho', 15.8673611, -97.099339, 200, 7), -- parapente
-- 8  ('Parques acuáticos'),
  ('El Rollo Yautepec', 18.6344053,-99.1595089, 300, 8),
  ('Hurricane Harbor', 18.900321,-98.9811056, 450, 8),
  ('Parque acuatico Ventanilla', 15.86676,-97.1006103, 200, 8),
  ('Splash Tangamanga', 22.1234742,-101.0257839, 200, 8), -- parque acuatico,  Pueblo magico 5
-- 9  ('Ríos'),
  ('Rio Tecolutla', 20.4765302,-97.0239487, 0, 9),
  ('Rio Maitines', 22.3762852,-99.3059942, 0, 9 ),
  ('Puente de Dios Tamasopo', 21.931081,-99.4000327, 50, 9 ),
  ('Rio escamilla', 21.1962008,-99.6065495, 35, 9),
  ('Rio Chuveje', 21.1712712,-99.5644281, 40, 9),
  ('Parque Nacional Gogorrón', 21.9860884,-101.0033143, 100, 9), -- rio
  ('Laguna Manialtepec', 15.9393611,-97.2121151, 100, 9), -- rio, Lago 7
  ('Parque Nacional Lagunas de Chacahua', 15.9809872,-97.7227507, 100, 9), -- rio
-- 10  ('Cenotes'),
  ('Cenote Xbatun', 20.501694,-89.8884795, 50, 10),
  ('Cenote Kankirixche', 20.501694,-89.8884795, 80, 10),
  ('Cenote Xkeken y Saluma', 20.501694,-90.7289336, 90, 10),
  ('Playa Bacocho', 15.8673611, -97.099339, 100, 10), -- cenote, Playa 1
-- 11  ('Caminatas'),
  ('Caminata Mineral del Chico', 20.2133761,-98.7349602, 100, 11),
  ('Caminata Tepozteco', 18.9975051,-99.0997223, 0, 11),
  ('Parque Nacional Gogorrón', 21.9860884,-101.0033143, 100, 11), -- caminata
  ('Parque Nacional Lagunas de Chacahua', 15.9809872,-97.7227507, 100, 11), -- caminata
-- 12  ('Ruinas aqueológicas'),
  ('Cacaxtla', 19.245683,-98.3582651, 20, 12), -- ruinas arq, Pueblo magico 5
  ('Teotihuacan', 19.6899771,-98.8453326, 80, 12),
  ('Tzintzuntzan', 19.6292327,-101.5815192, 45, 12),
  ('Monte Alban', 17.0438035,-96.7702991, 40, 12),
-- 13  ('Museos'),
  ('Campanario de la Catedral Metropolitana', 19.4343942, -99.1352711, 20, 13), -- ¿Museo?
  ('Catacumbas de la Catedral Metropolitana', 19.4343942, -99.1352711, 40, 13), -- ¿Museo?
  ('Cacaxtla', 19.245683,-98.3582651, 20, 13), -- museo
  ('Museo Laberinto de las Ciencias y las Artes', 22.103101,-101.0133512, 50, 13), -- museo, Ciudad 6
  ('Museo de Arte Contemporáneo', 22.1549425,-100.9984263, 20, 13), -- museo, Ciudad 6
  ('Museo del Ferrocarril', 22.157503,-100.9724296, 100, 13), -- museo, Ciudad 6
  ('Catedral Metropolitana San Luis Potosí', 22.1519652,-100.9774992, 0, 13), -- museo, Ciudad 6
-- 14  ('Minas'),
  ('Mina el Eden', 22.7292652,-102.8353003, 100, 14),
  ('Mina Socavon', 19.799804,-100.1498739, 150, 14),
  ('Mina las dos estrellas',19.7928429,-100.1580338,80, 14),
-- 15  ('Monumentos históricos');
  ('Ca	tedral Metropolitana', 19.4343942, -99.1352711, 0, 15), -- monumento histórico
  ('Catedral Metropolitana San Luis Potosí', 22.1519652,-100.9774992, 0, 15), -- monumento, Ciudad 6
  ('Catedral de Puerto Escondido', 15.8677984,-97.0744316, 0, 15), -- monumento, Playa 1
  -- 18 Mirador
  ('Espinazo del diablo', 23.6633591,-105.7676802, 0,18 ),
  ('La Rumorosa', 32.1165141,-115.9897621, 0,18);

  -- +--------------+---------------+
  -- |            1 | Playa         |
  -- |            2 | Montaña       |
  -- |            3 | Desierto      |
  -- |            4 | Selva         |
  -- |            5 | Pueblo Mágico |
  -- |            6 | Ciudad        |
  -- |            7 | Lago
  --              8 | Pueblo
  --              9 | Bosque|
  -- +--------------+---------------+
insert into atraccion_zona (atraccion_id, tipo_zona_id) values
  -- De primer conjunto, datos de Diana
  (1,6),
  (2,4),
  (3,4),
  (4,1),
  (5,1),
  (6,4),
  (7,4),
  (8,4),
  (9,4),
  (10,7),
  -- De segundo conjunto
	(11, 5),
	(12, 5),
	(13, 8),
	(14, 5),
	(15, 5),
	(16, 8),
	(17, 8),
	(18, 1),
	(19, 7),
	(20, 1),
	(20, 7),
	(21, 4),
	(22, 6),
	(23, 6),
	(24, 6),
	(25, 6),
	(26, 6),
	(27, 1),
	(28, 2),
	(28, 9),
	(29, 2),
	(29, 9),
	(30, 4),
	(31, 6),
	(32, 9),
	(33, 8),
	(34, 6),
	(35, 7),
	(35, 8),
	(36, 7),
	(36, 8),
	(37, 6),
	(38, 1),
	(38, 6),
	(39, 2),
	(40, 2),
	(41, 9),
	(42, 1),
	(43, 8),
	(44, 8),
	(45, 4),
	(46, 5),
	(47, 9),
	(48, 9),
	(49, 4),
	(50, 9),
	(51, 9),
	(52, 9),
	(53, 7),
	(53, 8),
	(54, 7),
	(55, 4),
	(56, 4),
	(57, 4),
	(58, 1),
	(59, 5),
	(60, 5),
	(61, 9),
	(62, 7),
	(63, 5),
	(64, 3),
	(64, 8),
	(65, 7),
	(65, 8),
	(66, 2),
	(67, 6),
	(68, 6),
	(69, 5),
	(70, 6),
	(71, 6),
	(72, 6),
	(73, 6),
	(74, 2),
	(74, 9),
	(75, 2),
	(76, 2),
	(77, 6),
	(78, 6),
	(79, 1),
	(80, 2),
	(81, 2);

insert into hospedaje(nombre,latitud,longitud,costo) values
  ('New Moon Hostel',20.9740147,-89.6199028,300),
  ('Hotel Zama',20.9718107,-89.6314041,1300),
  ('Hotel Dolores Alba',20.9605995,-89.6249902,740),
  ('Hotel Celestun',20.8618655,-90.4005102,500),
  ('Hotel Villa Calakmul',18.5084659,-89.400154,450),
  ('Hotel Maya Balam',18.5105107,-89.4050385,750),
  ('Hotel Laguna Bacalar',18.6724031,-88.4284097,1650),
  ('The Yak Lake House',18.6736391,-88.3996486,723),
  ('Blue Hotel',18.6803906,-88.3976762,809),
  ('Piedra de Agua Hotel Boutique',20.968721,-89.6250842,1233),
  -- segundo grupo de hospedajes
  ('Casa San Ildefonso',19.4360824,-99.1328663,800),
  ('Holiday Inn & Suites Merida La Isla',21.0991311,-89.6322378,1450), -- 3 y 4
  ('Hotel Boutique Casa de Campo Conkal',21.0750498,-89.6949681,1770),
  ('Hotel Palacio Maya',21.0750498,-89.6949681,900),
  ('Hotel Maria del Carmen',20.8593328,-90.402546,800),
  ('Xixim',20.868448,-90.3971235,400), -- 1, 4
  ('Cabañas Ek Balam',20.8922985,-88.1381227,600), -- 1,2
  ('Cabañas y Hostal Zoh Laguna',18.8581364,-89.7986624,460), -- 1,2,4
  ('Blue Hotel', 18.6698874,-88.4343301, 680), -- 2, 4
  ('Hotel Zacatlán',19.9331805,-97.962916,800),
  ('Posada Teyecac en Teotihuacan',19.6683507,-98.8861276,660), -- 2,1
  ('BEST NIGHT HOTEL LA LAGUNA',15.9738225,-97.2784797,1000), -- 1,4
  ('Hotel Mision Express San Luis Potosi',21.9902535,-101.0022941,950), --
  ('Staybridge Suites San Luis Potosi',21.9902535,-101.0022941,1500), -- 3
  ('Hotel Boutique Santa Lucía',21.5512668,-99.3586938,1111),
  ('HOTEL MORENO',21.8230721,-99.340841,650),
  ('Hotel Mision Jalpan',21.8230721,-100.181295,800),
  ('Radisson Poliforum Plaza Hotel Leon',21.1637362,-101.5820519,1700), -- 3 4
  ('Hotel Casa Blanca',21.3224534,-104.5154015,350), -- 4, 2
  ('HS HOTSSON Hotel Leon',21.1064153,-101.773626,2400),
  ('Gamma León',21.1064153,-101.773626,830),
  ('La Escondida Hostel',15.8545176,-97.1246213,300), -- 2
  ('Hotel Suites Villasol',15.8545176,-97.1246213,1400),
  ('Hotel Gemelas',15.9772139,-97.5833774,250),
  ('Cabañas La Isla Chacahua',15.9673118,-97.6626849,820), -- 1 4
  ('Holbox Dream',20.3922617,-87.8096012,2500);

-- +--------------+---------------+
  -- |            1 | Campamento       |
  -- |            2 | Hostal           |
  -- |            3 | Todo incluid     |
  -- |            4 | Hotel            |
  -- +--------------+---------------+
insert into hospedaje_tipos (hospedaje_id, tipo_hospedaje_id) values
  (1, 2),
  (2, 4),
  (3, 4),
  (4, 4),
  (5, 4),
  (6, 4),
  (7, 4),
  (8, 2),
  (9, 4),
  (10, 4);
